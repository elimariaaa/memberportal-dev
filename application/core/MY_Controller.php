<?php defined('BASEPATH') OR exit('No direct script access allowed');
 
class MY_Controller extends CI_Controller
{
    protected $data = array();
    function __construct()
    {
        parent::__construct();
        $this->data['pagetitle'] = 'Brunchwork Member Portal';
        $this->load->model('UserModel');
    }
 
    protected function render($the_view = NULL, $template = 'memberportal')
    {
        if($template == 'json' || $this->input->is_ajax_request())
        {
            header('Content-Type: application/json');
            echo json_encode($this->data);
        }
        elseif(is_null($template))
        {
            $this->load->view($the_view,$this->data);
        }
        else
        {
            if($this->ion_auth->logged_in()){
                $this->data['industry'] = $this->UserModel->get_industry();
                $this->data['members'] = $this->UserModel->get_members();
                $this->data['city'] = (!$this->UserModel->get_city()) ? 'N/A': $this->UserModel->get_city()->city;
            }
            $this->data['the_view_content'] = (is_null($the_view)) ? '' : $this->load->view($the_view,$this->data, TRUE);;
            $this->load->view('templates/'.$template.'_main', $this->data);
        }
    }
}

class Auth_Controller extends MY_Controller {
    function __construct() {
        parent::__construct();
        $this->load->library('ion_auth');
        if($this->ion_auth->logged_in()===FALSE)
        {
            redirect('login');
        }
    }
    protected function render($the_view = NULL, $template = 'auth')
    {
        parent::render($the_view, $template);
    }
}