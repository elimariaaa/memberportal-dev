<html>
<body>
	<!--
	<h1><?php echo sprintf(lang('email_forgot_password_heading'), $identity);?></h1>
	<p><?php echo sprintf(lang('email_forgot_password_subheading'), anchor('member/login/reset_password/'. $forgotten_password_code, lang('email_forgot_password_link')));?></p>
	-->
	<p>Hello <strong><?php echo $first_name; ?></strong>,</p>
	<p>We have received a request to reset your member portal password. Please visit the link below<br />
	and  choose your new one:</p>
	<p style="color: #ffa500;"><strong><?php echo anchor('member/login/reset_password/'. $forgotten_password_code, lang('email_forgot_password_link')); ?></strong></p>
	<p>If you did not make this request or need assistance, please click <strong><a href="https://brunchwork.com/contact-us/" target="_blank">here</a></strong>.</p>
</body>
</html>