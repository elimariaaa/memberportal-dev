<?php
    $user = $this->ion_auth->user()->row();
    $city = $this->input->get('city');
    //print_r($user);
?>
<div class="twenty-spacer"></div>
<div class="text-center"><h1>Calendar</h1></div>
<!-- <div class="text-center"><p align="center"><span style="font-size:12px;color:red;">*(We only accept RSVPs for the current calendar month. Interested in an event next month? Check back in on the 1st.)</span></p></div> -->
<div class="text-center" style="display: none;"><p align="center"><span style="font-size:12px;">We only accept RSVPs for the current calendar month & events on the 1st to 5th of the next month. Interested in a later event? Check back in on the 1st.</span></p></div>
<div class="ten-spacer"></div>
<?php
    if($message){
?>
<div class="events_alert mb-2">
    <div class="alert alert-danger text-left city_alert" role="alert">
        <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <?php echo $message; ?>
    </div>
</div>
<?php
    }
?>
<div class="events_alert"></div>
<div class="events-body">
<div class="card-deck" style="display: none;">
    <div class="card col-md-12 bg-pink no-border">
        <div class="calendar-container container">
        <article class="d-flex justify-content-left">
        <div class="col-md-12 col-sm-12 col-xs-12">
            <div class="input-group input-group-lg col-md-5">
                <select class="form-control form-control-lg no-radius" id="member_city">
                    <option <?php echo ($city == 'all' || $city == '') ? 'selected':''; ?>data-search="all" value="" selected>All Cities</option>
                    <?php
                        $selected = '';
                    foreach($cities AS $show_city):
                        if($show_city['search'] == $city){
                            $selected = 'selected';
                        } else {
                            $selected = '';
                        }
                        echo "<option data-search='".$show_city['search']."' value='".$show_city['id']."' ".$selected.">".$show_city['city']."</option>";
                    endforeach;
                    ?>
                </select>
                <span class="input-group-append">
                    <button class="btn btn-outline-secondary search-membercity" type="button">
                        <i class="fa fa-filter"></i>
                    </button>
                </span>
            </div>
        </div>
        </div>
        </article>
    </div>
</div>
<div class="card-deck">
    <div class="card col-md-12 bg-pink no-border">
        <div class="card-body">
            <?php
                $csrf = array(
                        'name' => $this->security->get_csrf_token_name(),
                        'hash' => $this->security->get_csrf_hash()
                    );
            ?>
            <?php
                //$request_url = "https://www.eventbriteapi.com/v3/events/search/?organizer.id=".$organizer_id."&token=".$token;
            $formatted_date =  (new \DateTime('America/New_York'))->format('Y-m-d\T00:00:00');
                
                    //$request_url = "https://www.eventbriteapi.com/v3/users/me/owned_events/?order_by=start_asc&status=live&expand=venue&token=".EVENT_TOKEN;
                    $request_url = "https://www.eventbriteapi.com/v3/organizations/".ORGANIZER_ID."/events/?status=live&token=".EVENT_TOKEN;
                
               // print_r($request_url); //exit;
                $params = array('sort_by' => 'date', 'organizer.id' => EVENT_ORGANIZER, 'token' => EVENT_TOKEN);
                $context = stream_context_create(
                                array(
                                    'http' => array(
                                        'method'  => 'GET',
                                        'header'  => "Content-type: application/x-www-form-urlencoded\r\n"//,
                                        //'content' => http_build_query($params)
                                    )
                                )
                            );
                $json_data = file_get_contents( $request_url, false, $context );
                //$response = get_object_vars(json_decode($json_data));
                $response = json_decode($json_data, true);
                $get_count = $response['pagination']['object_count'];

                for($x = 0; $x < $get_count; $x++){
                    if($response['events'][$x]['listed'] == true){
                    $event_date = date('Y-M-d', strtotime($response['events'][$x]['start']['local']));
                    $start_event_time = date('g:i A', strtotime($response['events'][$x]['start']['local']));
                    $end_event_time = date('g:i A', strtotime($response['events'][$x]['end']['local']));
                    $day = date('l', strtotime($response['events'][$x]['start']['local']));
                    $end_day = date('l', strtotime($response['events'][$x]['end']['local']));
                    $calendar = explode('-', $event_date);
                    $whole_date = date('F j, Y', strtotime($response['events'][$x]['start']['local']));
                    $end_whole_date = date('F j, Y', strtotime($response['events'][$x]['end']['local']));
                    $tz = new DateTimeZone($response['events'][$x]['start']['timezone']);
                    $get_timezone = new DateTime($response['events'][$x]['start']['local'],$tz);
                    $time_zone_string = $get_timezone->format('T');
                    $calendar_date = $calendar[2];
                    $calendar_month = $calendar[1];
                    $date_msg = '';
                    if($whole_date == $end_whole_date){
                        $date_msg = $day.', '.$whole_date.' at '.$start_event_time.' - '.$end_event_time;
                    } else {
                        $date_msg = $day.', '.$whole_date.' at '.$start_event_time.' - '.$end_day.', '.$end_whole_date.' at '.$end_event_time;
                    }
                    //get speaker name
                    $description_url = 'https://www.eventbriteapi.com/v3/events/'.$response['events'][$x]['id'].'/description?token='.EVENT_TOKEN;
                    $desc_json_data = file_get_contents( $description_url, false, $context );
                    $desc_response = json_decode($desc_json_data, true);
                    $html = str_get_html($desc_response['description']);
                    $custom_url = '';
                    $custom_url_arr = array();
					if($response['events'][$x]['id'] == '106012845274' || $response['events'][$x]['id'] == '105529487538' || $response['events'][$x]['id'] == '105091294892'){
						$custom_url= $html->find('#speaker_name') ? $html->find('#speaker_name', 0)->innertext : '';
					} else {
						foreach($html->find('div') AS $get_test){
							if($get_test->find('img')){
                                if($get_test->nextSibling()){
                                    if($get_test->nextSibling()->innertext){
                                        $speaker_name = str_replace('Speaker Name: ', '', $get_test->nextSibling()->find('p',0)->innertext);
                                        $custom_url_arr[] = str_replace(' ', '_', strtolower($speaker_name));
                                    }
                                }
							}
						}
						
						foreach($custom_url_arr AS $index => $custom_url_data){
							if($index != 0){
								$custom_url .= '_'.$custom_url_data;
							} else {
								$custom_url .= $custom_url_data;
							}
						}
					}
                    ?>
                    <?php if($city == "newyork" ){
                    if($response['events'][$x]['venue']['address']['city'] == 'New York') {  ?>
                    <div class="calendar-container container">
                        <article id="event-<?php echo $response['events'][$x]['id']; ?>" class="d-flex justify-content-center"> 
                            <div class="calendar col-md-12 col-sm-12 col-xs-12 row">
                                <div class="date active col-md-1 col-sm-12 col-xs-12">
                                    <p class="calendar-date"><?php echo $calendar_date; ?></p>
                                    <p class="calendar-month"><?php echo $calendar_month; ?></p>
                                </div>
                                <div class="cal-img col-md-6 col-sm-12 col-xs-12">
                                    <a style="cursor: default;" class="post-thumbnail" href="javascript:void(0);" class="wp-post-image"  >
                                    <img src="<?php echo $response['events'][$x]['logo']['original']['url']; ?>" class="img-fluid" />
                                    </a>
                                </div>
                                <div class="cal-desc col-md-5 col-sm-12 col-xs-12"> 
                                    <h4 class="calendar-date text-center"><a href="<?php echo base_url('events/details/'.$custom_url.'/'.$response['events'][$x]['id']); ?>" target="_blank" data-region= "<?php echo $response['events'][$x]['venue']['address']['region']; ?>" data-location= "<?php echo $response['events'][$x]['venue']['address']['city']; ?>" data-event_id = "<?php echo $response['events'][$x]['id']; ?>"><?php echo $response['events'][$x]['name']['text']; ?></a></h4>
                                    <div class="col text-center rsvp-button">
                                        <?php
                                        if($response['events'][$x]['online_event'] == true){
                                        ?>
                                        <h5>Online Event</h5>
                                        <div class="twenty-spacer"></div>
                                        <?php
                                        } else {
                                        ?>
                                        <h5><?php echo $response['events'][$x]['venue']['address']['city']; ?></h5>
                                        <h5><?php echo $response['events'][$x]['venue']['name']; ?></h5>
                                        <?php
                                        }
                                        ?>
                                        <h6 class="calendar-month"><?php echo $date_msg. ' ('.$time_zone_string.')'; ?></h6>
                                        <!--<h6 class="calendar-month"><?php echo $start_event_time.' to '.$end_event_time; ?></h6>-->
                                        <?php
                                            if($user->eventbrite_token){
                                                $eventbrite_url = $response['events'][$x]['url'];
                                            } else {
                                                $eventbrite_url = "https://www.eventbrite.com/oauth/authorize?response_type=code&client_id=".EVENTBRITE_APP_KEY;
                                            }
                                        ?>
                                        <!--href="<?php echo $eventbrite_url; ?>"-->
                                        <div class="event_description" style="display: none;">
                                            <?php echo $response['events'][$x]['description']['html']; ?>
                                        </div>
                                        <input type="hidden" name="event_name" class="event_name" value="<?php echo $response['events'][$x]['name']['text']; ?>" />
                                        <div class="row">
                                            <div class="col-xs-12 col-sm-4 mb-1 pl-0 pr-1">
                                                <button type="button" data-event_id = "<?php echo $response['events'][$x]['id'];?>" class="btn btn-brunchwork btn-events attendee-list">Attendee List</button>
                                            </div>
                                            <?php
                                                if( in_array( $response['events'][$x]['id'] ,$rsvpd_events ) )
                                                {
                                            ?>
                                            <div class="col-xs-12 col-sm-4 mb-1 pl-0 pr-1">
                                            <button type="button" class="btn btn-events btn-brunchwork edit_guests_button" data-event_id = "<?php echo $response['events'][$x]['id']; ?>">Edit Guests</button>
                                            </div>
                                            <div class="col-xs-12 col-sm-4 mb-1 pl-0 pr-1">
                                            <button type="button" class="btn btn-brunchwork btn-events rsvp_cancel_button" data-event_id = "<?php echo $response['events'][$x]['id']; ?>">Cancel RSVP</button>
                                            </div>
                                            <?php
                                                } else {
                                            ?>
                                             <div class="col-xs-12 col-sm-4 mb-1 pl-0 pr-1">
                                                <a href="<?php echo base_url('events/details/'.$custom_url.'/'.$response['events'][$x]['id']); ?>" target="_blank" class="btn btn-brunchwork btn-events" data-region= "<?php echo $response['events'][$x]['venue']['address']['region']; ?>" data-location= "<?php echo $response['events'][$x]['venue']['address']['city']; ?>" data-event_id = "<?php echo $response['events'][$x]['id']; ?>">Details</a>
                                            </div>
                                            <div class="col-xs-12 col-sm-4 mb-1 pl-0 pr-1">
                                                <button type="button" class="btn btn-brunchwork btn-events rsvp_button" data-calendar_day = "<?php echo $calendar_date; ?>" data-calendar_month = "<?php echo $calendar_month; ?>" data-event_id = "<?php echo $response['events'][$x]['id']; ?>">RSVP</button>
                                            </div>
                                            <?php 
                                                }
                                            ?>
                                            <div class="row">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                        </article>
                    </div>
                    <?php   }} 


                    elseif($city == "sanfrancisco" ){
                    if($response['events'][$x]['venue']['address']['city'] == 'San Francisco') {  ?>
                    <div class="calendar-container container">
                        <article id="event-<?php echo $response['events'][$x]['id']; ?>" class="d-flex justify-content-center"> 
                            <div class="calendar col-md-12 col-sm-12 col-xs-12 row">
                                <div class="date active col-md-1 col-sm-12 col-xs-12">
                                    <p class="calendar-date"><?php echo $calendar_date; ?></p>
                                    <p class="calendar-month"><?php echo $calendar_month; ?></p>
                                </div>
                                <div class="cal-img col-md-6 col-sm-12 col-xs-12">
                                    <a style="cursor: default;" class="post-thumbnail" href="javascript:void(0);" class="wp-post-image"  >
                                    <img src="<?php echo $response['events'][$x]['logo']['original']['url']; ?>" class="img-fluid" />
                                    </a>
                                </div>
                                <div class="cal-desc col-md-5 col-sm-12 col-xs-12"> 
                                    <h4 class="calendar-date text-center"><a href="<?php echo base_url('events/details/'.$custom_url.'/'.$response['events'][$x]['id']); ?>" target="_blank" data-region= "<?php echo $response['events'][$x]['venue']['address']['region']; ?>" data-location= "<?php echo $response['events'][$x]['venue']['address']['city']; ?>" data-event_id = "<?php echo $response['events'][$x]['id']; ?>"><?php echo $response['events'][$x]['name']['text']; ?></a></h4>
                                    <div class="col text-center rsvp-button">
                                        <?php
                                        if($response['events'][$x]['online_event'] == true){
                                        ?>
                                        <h5>Online Event</h5>
                                        <div class="twenty-spacer"></div>
                                        <?php
                                        } else {
                                        ?>
                                        <h5><?php echo $response['events'][$x]['venue']['address']['city']; ?></h5>
                                        <h5><?php echo $response['events'][$x]['venue']['name']; ?></h5>
                                        <?php
                                        }
                                        ?>
                                        <h6 class="calendar-month"><?php echo $date_msg. ' ('.$time_zone_string.')'; ?></h6>
                                        <!--<h6 class="calendar-month"><?php echo $start_event_time.' to '.$end_event_time; ?></h6>-->
                                        <?php
                                            if($user->eventbrite_token){
                                                $eventbrite_url = $response['events'][$x]['url'];
                                            } else {
                                                $eventbrite_url = "https://www.eventbrite.com/oauth/authorize?response_type=code&client_id=".EVENTBRITE_APP_KEY;
                                            }
                                        ?>
                                        <!--href="<?php echo $eventbrite_url; ?>"-->
                                        <div class="event_description" style="display: none;">
                                            <?php echo $response['events'][$x]['description']['html']; ?>
                                        </div>
                                        <input type="hidden" name="event_name" class="event_name" value="<?php echo $response['events'][$x]['name']['text']; ?>" />
                                        <div class="row">
                                            <div class="col-xs-12 col-sm-4 mb-1 pl-0 pr-1">
                                                <button type="button" data-event_id = "<?php echo $response['events'][$x]['id'];?>" class="btn btn-brunchwork btn-events attendee-list">Attendee List</button>
                                            </div>
                                            <?php
                                                if( in_array( $response['events'][$x]['id'] ,$rsvpd_events ) )
                                                {
                                            ?>
                                            <div class="col-xs-12 col-sm-4 mb-1 pl-0 pr-1">
                                            <button type="button" class="btn btn-events btn-brunchwork edit_guests_button" data-event_id = "<?php echo $response['events'][$x]['id']; ?>">Edit Guests</button>
                                            </div>
                                            <div class="col-xs-12 col-sm-4 mb-1 pl-0 pr-1">
                                            <button type="button" class="btn btn-brunchwork btn-events rsvp_cancel_button" data-event_id = "<?php echo $response['events'][$x]['id']; ?>">Cancel RSVP</button>
                                            </div>
                                            <?php
                                                } else {
                                            ?>
                                             <div class="col-xs-12 col-sm-4 mb-1 pl-0 pr-1">
                                                <a href="<?php echo base_url('events/details/'.$custom_url.'/'.$response['events'][$x]['id']); ?>" target="_blank" class="btn btn-brunchwork btn-events" data-region= "<?php echo $response['events'][$x]['venue']['address']['region']; ?>" data-location= "<?php echo $response['events'][$x]['venue']['address']['city']; ?>" data-event_id = "<?php echo $response['events'][$x]['id']; ?>">Details</a>
                                            </div>
                                            <div class="col-xs-12 col-sm-4 mb-1 pl-0 pr-1">
                                                <button type="button" class="btn btn-brunchwork btn-events rsvp_button" data-calendar_day = "<?php echo $calendar_date; ?>" data-calendar_month = "<?php echo $calendar_month; ?>" data-event_id = "<?php echo $response['events'][$x]['id']; ?>">RSVP</button>
                                            </div>
                                            <?php 
                                                }
                                            ?>
                                            <div class="row">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                        </article>
                    </div>
                    <?php   }} 



                    elseif($city == "losangeles" ){
                    if($response['events'][$x]['venue']['address']['city'] == 'Los Angeles') {  ?>
                    <div class="calendar-container container">
                        <article id="event-<?php echo $response['events'][$x]['id']; ?>" class="d-flex justify-content-center"> 
                            <div class="calendar col-md-12 col-sm-12 col-xs-12 row">
                                <div class="date active col-md-1 col-sm-12 col-xs-12">
                                    <p class="calendar-date"><?php echo $calendar_date; ?></p>
                                    <p class="calendar-month"><?php echo $calendar_month; ?></p>
                                </div>
                                <div class="cal-img col-md-6 col-sm-12 col-xs-12">
                                    <a style="cursor: default;" class="post-thumbnail" href="javascript:void(0);" class="wp-post-image"  >
                                    <img src="<?php echo $response['events'][$x]['logo']['original']['url']; ?>" class="img-fluid" />
                                    </a>
                                </div>
                                <div class="cal-desc col-md-5 col-sm-12 col-xs-12"> 
                                    <h4 class="calendar-date text-center"><a href="<?php echo base_url('events/details/'.$custom_url.'/'.$response['events'][$x]['id']); ?>" target="_blank" data-region= "<?php echo $response['events'][$x]['venue']['address']['region']; ?>" data-location= "<?php echo $response['events'][$x]['venue']['address']['city']; ?>" data-event_id = "<?php echo $response['events'][$x]['id']; ?>"><?php echo $response['events'][$x]['name']['text']; ?></a></h4>
                                    <div class="col text-center rsvp-button">
                                        <?php
                                        if($response['events'][$x]['online_event'] == true){
                                        ?>
                                        <h5>Online Event</h5>
                                        <div class="twenty-spacer"></div>
                                        <?php
                                        } else {
                                        ?>
                                        <h5><?php echo $response['events'][$x]['venue']['address']['city']; ?></h5>
                                        <h5><?php echo $response['events'][$x]['venue']['name']; ?></h5>
                                        <?php
                                        }
                                        ?>
                                        <h6 class="calendar-month"><?php echo $date_msg. ' ('.$time_zone_string.')'; ?></h6>
                                        <!--<h6 class="calendar-month"><?php echo $start_event_time.' to '.$end_event_time; ?></h6>-->
                                        <?php
                                            if($user->eventbrite_token){
                                                $eventbrite_url = $response['events'][$x]['url'];
                                            } else {
                                                $eventbrite_url = "https://www.eventbrite.com/oauth/authorize?response_type=code&client_id=".EVENTBRITE_APP_KEY;
                                            }
                                        ?>
                                        <!--href="<?php echo $eventbrite_url; ?>"-->
                                        <div class="event_description" style="display: none;">
                                            <?php echo $response['events'][$x]['description']['html']; ?>
                                        </div>
                                        <input type="hidden" name="event_name" class="event_name" value="<?php echo $response['events'][$x]['name']['text']; ?>" />
                                        <div class="row">
                                            <div class="col-xs-12 col-sm-4 mb-1 pl-0 pr-1">
                                                <button type="button" data-event_id = "<?php echo $response['events'][$x]['id'];?>" class="btn btn-brunchwork btn-events attendee-list">Attendee List</button>
                                            </div>
                                            <?php
                                                if( in_array( $response['events'][$x]['id'] ,$rsvpd_events ) )
                                                {
                                            ?>
                                            <div class="col-xs-12 col-sm-4 mb-1 pl-0 pr-1">
                                            <button type="button" class="btn btn-events btn-brunchwork edit_guests_button" data-event_id = "<?php echo $response['events'][$x]['id']; ?>">Edit Guests</button>
                                            </div>
                                            <div class="col-xs-12 col-sm-4 mb-1 pl-0 pr-1">
                                            <button type="button" class="btn btn-brunchwork btn-events rsvp_cancel_button" data-event_id = "<?php echo $response['events'][$x]['id']; ?>">Cancel RSVP</button>
                                            </div>
                                            <?php
                                                } else {
                                            ?>
                                             <div class="col-xs-12 col-sm-4 mb-1 pl-0 pr-1">
                                                <a href="<?php echo base_url('events/details/'.$custom_url.'/'.$response['events'][$x]['id']); ?>" target="_blank" class="btn btn-brunchwork btn-events" data-region= "<?php echo $response['events'][$x]['venue']['address']['region']; ?>" data-location= "<?php echo $response['events'][$x]['venue']['address']['city']; ?>" data-event_id = "<?php echo $response['events'][$x]['id']; ?>">Details</a>
                                            </div>
                                            <div class="col-xs-12 col-sm-4 mb-1 pl-0 pr-1">
                                                <button type="button" class="btn btn-brunchwork btn-events rsvp_button" data-calendar_day = "<?php echo $calendar_date; ?>" data-calendar_month = "<?php echo $calendar_month; ?>" data-event_id = "<?php echo $response['events'][$x]['id']; ?>">RSVP</button>
                                            </div>
                                            <?php 
                                                }
                                            ?>
                                            <div class="row">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                        </article>
                    </div>
                    <?php   }} 


                    elseif($city == "all" || $city =" ") {
                        if($response['events'][$x]['venue_id'] != null){
                            $region = $response['events'][$x]['venue']['address']['region'];
                            $location = $response['events'][$x]['venue']['address']['city'];
                        } else {
                            $region = '';
                            if($response['events'][$x]['online_event'] == true)
                            $location =  'online';
                        }
                    ?>

                        <div class="calendar-container container">
                            <article id="event-<?php echo $response['events'][$x]['id']; ?>" class="d-flex justify-content-center"> 
                                <div class="calendar col-md-12 col-sm-12 col-xs-12 row">
                                    <div class="date active col-md-1 col-sm-12 col-xs-12">
                                        <p class="calendar-date"><?php echo $calendar_date; ?></p>
                                        <p class="calendar-month"><?php echo $calendar_month; ?></p>
                                    </div>
                                    <div class="cal-img col-md-6 col-sm-12 col-xs-12">
                                        <a style="cursor: default;" class="post-thumbnail" href="javascript:void(0);" class="wp-post-image"  >
                                        <img src="<?php echo $response['events'][$x]['logo']['original']['url']; ?>" class="img-fluid" />
                                        </a>
                                    </div>
                                    <div class="cal-desc col-md-5 col-sm-12 col-xs-12"> 
                                        <h4 class="calendar-date text-center"><a href="<?php echo base_url('events/details/'.$custom_url.'/'.$response['events'][$x]['id']); ?>" target="_blank" data-region= "<?php echo $region; ?>" data-location= "<?php echo $location; ?>" data-event_id = "<?php echo $response['events'][$x]['id']; ?>"><?php echo $response['events'][$x]['name']['text']; ?></a></h4>
                                        <div class="col text-center rsvp-button">
                                            <?php
                                            if($response['events'][$x]['online_event'] == true){
                                            ?>
                                            <h5>Online Event</h5>
                                            <div class="twenty-spacer"></div>
                                            <?php
                                            } else {
                                            ?>
                                            <h5><?php echo $response['events'][$x]['venue']['address']['city']; ?></h5>
                                            <h5><?php echo $response['events'][$x]['venue']['name']; ?></h5>
                                            <?php
                                            }
                                            ?>
                                            <h6 class="calendar-month"><?php echo $date_msg.'('.$time_zone_string.')'; ?></h6>
                                            <!--<h6 class="calendar-month"><?php echo $start_event_time.' to '.$end_event_time; ?></h6>-->
                                            <?php
                                                if($user->eventbrite_token){
                                                    $eventbrite_url = $response['events'][$x]['url'];
                                                } else {
                                                    $eventbrite_url = "https://www.eventbrite.com/oauth/authorize?response_type=code&client_id=".EVENTBRITE_APP_KEY;
                                                }
                                            ?>
                                            <!--href="<?php echo $eventbrite_url; ?>"-->
                                            <div class="event_description" style="display: none;">
                                                <?php echo $response['events'][$x]['description']['html']; ?>
                                            </div>
                                            <input type="hidden" name="event_name" class="event_name" value="<?php echo $response['events'][$x]['name']['text']; ?>" />
                                            <div class="row">
                                                <div class="col-xs-12 col-sm-4 mb-1 pl-0 pr-1">
                                                    <button type="button" data-event_id = "<?php echo $response['events'][$x]['id'];?>" class="btn btn-brunchwork btn-events attendee-list">Attendee List</button>
                                                </div>
                                                <?php
                                                    if( in_array( $response['events'][$x]['id'] ,$rsvpd_events ) )
                                                    {
                                                ?>
                                                <div class="col-xs-12 col-sm-4 mb-1 pl-0 pr-1">
                                                <button type="button" class="btn btn-events btn-brunchwork edit_guests_button" data-event_id = "<?php echo $response['events'][$x]['id']; ?>">Edit Guests</button>
                                                </div>
                                                <div class="col-xs-12 col-sm-4 mb-1 pl-0 pr-1">
                                                <button type="button" class="btn btn-brunchwork btn-events rsvp_cancel_button" data-event_id = "<?php echo $response['events'][$x]['id']; ?>">Cancel RSVP</button>
                                                </div>
                                                <?php
                                                    } else {
                                                ?>
                                                 <div class="col-xs-12 col-sm-4 mb-1 pl-0 pr-1">
                                                    <a href="<?php echo base_url('events/details/'.$custom_url.'/'.$response['events'][$x]['id']); ?>" target="_blank" class="btn btn-brunchwork btn-events" data-region= "<?php echo $region; ?>" data-location= "<?php echo $location; ?>" data-event_id = "<?php echo $response['events'][$x]['id']; ?>">Details</a>
                                                </div>
                                                <div class="col-xs-12 col-sm-4 mb-1 pl-0 pr-1">
                                                    <button type="button" class="btn btn-brunchwork btn-events rsvp_button" data-calendar_day = "<?php echo $calendar_date; ?>"  data-calendar_month = "<?php echo $calendar_month; ?>" data-event_id = "<?php echo $response['events'][$x]['id']; ?>">RSVP</button>
                                                </div>
                                                <?php 
                                                    }
                                                ?>
                                            </div>
                                        </div>
                                    </div>
                            </article>
                        </div>


                   <?php }  ?>
                    
                


                <?php }
                }
            ?>
        </div>
    </div>
</div>
</div>
<div class="center-parent loadingDiv"><div class="center-container"></div></div>
<script src="<?php echo base_url('assets/js/bootbox.min.js?v=').VER_NO; ?>"></script>
<script src="<?php echo base_url('assets/js/events.js?v=').VER_NO; ?>"></script>
<script type="text/javascript" src="<?php echo base_url('assets/js/settings.js?V='.VER_NO);?>"></script>