<?php
    $user = $this->ion_auth->user()->row();
    //print_r($user);
?>

<div class="twenty-spacer"></div>
<div class="container">
    <h1 style="text-align: center;">Member Directory</h1>
    <div class="text-center"><p align="center"><span style="font-size:13px;">Let us know your networking goals at <strong><a href="mailto:networking@brunchwork.com" style="color: #007bff;">networking@brunchwork.com</a></strong>. We’ll curate some introductions for you. </span></p></div>
    <div class="ten-spacer"></div>
    <div class="padding-topbottom container">
        <div class="row">
        <div class="col-md-6 mb-1">
            <div class="input-group input-group-lg">
                <input class="form-control no-radius py-2 memberdir-val example-search-input" type="search" placeholder="Search by Name" id="member_name">
                <span class="input-group-append">
                    <button class="btn btn-outline-secondary search-memberdir" type="button">
                        <i class="fa fa-search"></i>
                    </button>
                </span>
            </div>
        </div>
        <div class="col-md-6 mb-1">
            <div class="input-group input-group-lg">
                <!-- <input class="form-control no-radius py-2 member-indutry-val example-search-input" type="search" placeholder="Search by Industry" id="member_industry"> -->

                <select class="form-control form-control-lg no-radius py-2 member-indutry-val example-search-input" type="search" id="member_industry">
                    <option value="" selected disabled>Filter by Tags</option>
                    <option value="All">All</option>
                    <?php
                    //var_dump($tags); exit;
                    foreach($tags AS $tag):?>
                        <option value="<?php echo $tag; ?>"><?php echo $tag; ?></option>
                    <?php endforeach;
                    ?>
                </select>
                <span class="input-group-append">
                    <button class="btn btn-outline-secondary search-membertag" type="button">
                        <i class="fa fa-filter"></i>
                    </button>
                </span>
            </div>
        </div>
            <div class="col-md-6">
                <div class="input-group input-group-lg">
                    <!--
                <select class="form-control form-control-lg no-radius" id="member_plan">
                    <option value="" selected disabled>Select Plan.</option>
                    <?php
                    foreach($get_all_plans  AS $plans):
                        if(strpos($plans->id, 'sf-') !== false){
                            $plan_name = 'SF - '.$plans->name;
                        } else {
                            $plan_name = 'NYC - '.$plans->name;
                        }
                        echo "<option value=".$plans->id.">".$plan_name."</option>";
                    endforeach;
                    ?>
                </select>
            -->
                    <select class="form-control form-control-lg no-radius" id="member_city">

                        <option value="" selected disabled>Filter by City</option>
                        <option value="All">All</option>
                        <!--       <option value="" selected disabled>City</option>-->

                        <?php
                        //var_dump($tags); exit;
                        foreach($cities AS $show_city):
                            echo "<option value=".$show_city['id'].">".$show_city['city']."</option>";
                        endforeach;
                        ?>
                    </select>
                    <span class="input-group-append">
                    <button class="btn btn-outline-secondary search-membercity" type="button">
                        <i class="fa fa-filter"></i>
                    </button>
                </span>
                </div>
            </div>
        <div class="col-md-6">
            <div class="input-group input-group-lg">

                <select class="form-control form-control-lg no-radius" id="member_alumni">

                    <option value="" selected disabled>Filter by Status</option>
                    <option value="active">Active</option>
                    <option value="inactive">Alumni</option>
                </select>
                <span class="input-group-append">
                    <button class="btn btn-outline-secondary search-memberalumni" type="button">
                        <i class="fa fa-filter"></i>
                    </button>
                </span>
            </div>
        </div>

        <!--
        <div class="col-md-2">
			<button type="button" class="btn-lg btn-brunchwork btn memberdir_refresh"><i class="fa fa-redo"></i></button>
        </div>
        -->
        </div>
    </div>
    <div class="memberdirectory_holder"></div>
    
   <!-- Paginate -->
   <div style='margin-top: 10px;' id='pagination'></div>

</div>
<div class="hundred-spacer"></div>
<div class="center-parent loadingDiv"><div class="center-container"></div></div>
<script src="<?php echo base_url('assets/js/bootbox.min.js?v=').VER_NO; ?>"></script>
<script type="text/javascript" src="<?php echo base_url('assets/js/directory.js?V=').VER_NO; ?>"></script>
<script type="text/javascript" src="<?php echo base_url('assets/js/settings.js?V='.VER_NO);?>"></script>
