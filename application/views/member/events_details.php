<script src="<?php echo base_url('assets/js/jquery.fancybox.pack.js?v=').VER_NO; ?>"></script>
<script src="<?php echo base_url('assets/js/jquery.fancybox-media.js?v=').VER_NO; ?>"></script>
<link href="<?php echo base_url('assets/css/jquery.fancybox.css?v='.VER_NO); ?>" rel="stylesheet">
<link href="<?php echo base_url('assets/css/lightslider.css?v='.VER_NO); ?>" rel="stylesheet">
<script src="<?php echo base_url('assets/js/lightslider.js?v=').VER_NO; ?>"></script>
<script src="<?php echo base_url('assets/js/bwmailchimpplugin.js?v=').VER_NO; ?>"></script>
<script src="<?php echo base_url('assets/js/events.js?v=').VER_NO; ?>"></script>
<link href="https://fonts.googleapis.com/css?family=Open+Sans:400,700&display=swap" rel="stylesheet">

<style>
    
    .hide-error {
        display: none !important;
    }

    .has-error .checkbox, .has-error .checkbox-inline, .has-error .control-label, .has-error .help-block, .has-error .radio, .has-error .radio-inline, .has-error.checkbox label, .has-error.checkbox-inline label, .has-error.radio label, .has-error.radio-inline label {
        color: #a94442;
    }

    .help-block {
        text-align: center;
        border: 1px solid #ffa500;
        background: rgba(0,0,0,0.8);
        padding: 5px;
        clear: both;
        color: #fff;
        width: 90%;
        margin: 10px auto 10px;
        display: inline-block;
    }

    #containerforSubscribe .form-control {
        border-radius: 0px;
        box-shadow: none;
        height: 60px;
    }

    #containerforSubscribe select {
        padding: 1px 5px;
        font-weight: 500;
    }
    
    @media (min-width: 992px) {
        .signupFrom select {
            width: 100%;
        }

        #bwSubsemailFromGroup {
            width: 60%;
        }
    }

    .signupFrom select, .signupFrom select:focus {
        padding: 10px 20px 10px 5px !important;
        border: #a2a2a2 solid 0px !important;
        border-right: 0 !important;
        border-radius: 0;
        background: #ffa500 url('<?php echo base_url("assets/images/arrow-down.png"); ?>') 90% center no-repeat;
        color: #fff !important;
        font-size: 20px;
        appearance: none;
        -webkit-appearance: none;
        -moz-appearance: none;
        width: 100%;
        text-transform: uppercase;
        text-align: center;
        letter-spacing: -0.6px;
    }

    .signupFrom input[type="submit"], .signupFrom button {
        border: #a2a2a2 solid 0px;
        border-left: 0;
        margin: 0;
        border-radius: 0;
        padding: 14px;
        height: 60px;
        /* font-size: 11px; */
        font-weight: bold;
        text-transform: uppercase;
    }

    .wpcf7-submit {
        font-weight: bold !important;
    }
    .wpcf7-submit {
        width: auto;
        float: left;
        height: 45px;
        background-color: #ffa500;
        border-radius: 8px;
        border-color: none;
        border: none;
        color: #ffffff;
        font-size: 20px;
        text-align: center;
        cursor: pointer;
        margin-top: 10px;
    }

    #containerforSubscribe input, #containerforSubscribe select {
        border: 0px solid #ccc;
        margin-bottom: 0px;
    }

    @media screen and (max-width: 480px){
        .signupFrom .wpcf7-form-control-wrap.city {
            width: 19%;
        }

        .signupFrom .wpcf7-form-control-wrap.your-email {
            width: 53% !important;
        }

        .signupFrom input.wpcf7-form-control.wpcf7-submit, .signupFrom input.wpcf7-form-control.wpcf7-submit, .signupFrom button {
            width: 28% !important;
        }

        #containerforSubscribe input, #containerforSubscribe select, .signupFrom input.wpcf7-form-control.wpcf7-submit, .signupFrom input.wpcf7-form-control.wpcf7-submit, .signupFrom button {
            font-size: 11px;
        }
    }

    @media only screen and (min-width: 981px) {
        .join-party-content .signupFrom input[type="email"] {
            border: #333 solid 2px !important;
        }

        .join-party-content .signupFrom select, .join-party-content .signupFrom input[type="submit"] {
            border: #ffa500 solid 3px !important;
        }
    }
    
    .signupFrom input[type="email"] {
        background: url('<?php echo base_url("assets/images/bg-input.png"); ?>') repeat;
        border-radius: 0;
        border-top: 0px #a2a2a2 solid!important;
        border-bottom: 0px #a2a2a2 solid!important;
        border-left: 0 !important;
        border-right: 0 !important;
        color: #fff;
        font-size: 20px;
        font-family: 'Open Sans', Arial, sans-serif;
        width: 100%;
        height: 61px;
        padding: 14px;
        text-align: left;
    }
    @media (min-width: 768px) {

        .signupFrom input[type="email"] {
            width: 100%;
        }

        .join-party-content .signupFrom {
            margin-top: 20px;
        }

        .signupFrom {
            width: 88%;
            margin: 30px auto 0;
        }

        .city {
            width: 20%;
        }

        .signupFrom select {
            padding: 14px 30px 14px 8px !important;
            height: 61px;
            /* width: 130px; */
        }
    }

    .signupFrom {
        margin-top: 10px;
    }
    .signupFrom .wpcf7-form-control-wrap {
        float: left;
    }
    
    .wpcf7-form-control-wrap {
        position: relative;
    }

    div {
        font-family: 'Open Sans', Arial, sans-serif;
    }

    .join-community {
        line-height: 1.5em;
    }
    .join-community {
        background: url('<?php echo base_url("assets/images/bg-join-community.png"); ?>') repeat;
        display: inline-block;
        font-weight: bold;
        font-size: 18px;
        padding: 3px 15px 0px;
        margin: 7px 0 15px;
        text-transform: uppercase;
        color: #000;
    }

    h1, h2, h3, h4, h5, h6 {
        padding-bottom: 10px;
    }

    h4 {
        font-size: 18px;
    }

    
    div.container{
        max-width: 100%;
        padding: 0;
        margin-right: auto;
        margin-left: auto;
    }
    ul{
        list-style: none outside none;
        padding-left: 0;
        margin: 0;
    }
    .demo .item{
        margin-bottom: 60px;
    }
    .content-slider li{
        /*background-color: #ed3020;*/
        text-align: center;
        color: #FFF;
    }
    .content-slider h3 {
        margin: 0;
        padding: 70px 0;
    }
    .demo{
        width: 800px;
    }

    table#as_featured_in td {
        border: none;
    }

    ul.lSPager.lSpg {
        display: none;
    }

    .row {
        margin-right: 0px;
    }

    p.secret_weapon {
        font-size: 14px;
    }

    #bw_foot_a: hover{
        text-decoration: underline;
    }

    nav#event_details_footer li.nav-item {
        padding-left: 20px;
        font-size: 14px;
    }

    li.list-inline-item {
        font-size: 14px;
    }

    nav#event_details_footer li.nav-item a.no-decor, li.list-inline-item a.no-decor {
        text-decoration: none;
    }

    nav#event_details_footer li.nav-item a, li.list-inline-item a {
        color: #7d7e7f;
        text-transform: uppercase;
    }

    nav#event_details_footer li.nav-item a:hover, li.list-inline-item a:hover {
        opacity: 0.7;
        text-decoration: underline;
    }
    
    #footer-info a:hover {
        opacity: 0.7;
        text-decoration: underline;
    }

    #footer-info {
        font-size: 15px;
        color: #fff;
    }
</style>
<?php
    //$event_id = $this->input->get('id');
    //$request_url = "https://www.eventbriteapi.com/v3/events/".$this->input->get('id')."?organizer.id=".EVENT_ORGANIZER."&token=".EVENT_TOKEN;
    $request_url = "https://www.eventbriteapi.com/v3/events/".$event_id."?expand=venue&token=".EVENT_TOKEN;
    $description_url = "https://www.eventbriteapi.com/v3/events/".$event_id."/description/?token=".EVENT_TOKEN;
    $params = array('sort_by' => 'date', 'organizer.id' => EVENT_ORGANIZER, 'token' => EVENT_TOKEN);
    $context = stream_context_create(
                    array(
                        'http' => array(
                            'method'  => 'GET',
                            'header'  => "Content-type: application/x-www-form-urlencoded\r\n"//,
                            //'content' => http_build_query($params)
                        )
                    )
                );
    $json_data = file_get_contents( $request_url, false, $context );
    $desc_json_data = file_get_contents( $description_url, false, $context );
    //$response = get_object_vars(json_decode($json_data));
    $response = json_decode($json_data, true);
    $desc_response = json_decode($desc_json_data, true);
    //echo "<pre>";
    //print_r($response);
    $event_date = date('Y-M-d', strtotime($response['start']['local']));
    $start_event_time = date('g:i A', strtotime($response['start']['local']));
    $end_event_time = date('g:i A', strtotime($response['end']['local']));
    $day = date('l', strtotime($response['start']['local']));
    $end_day = date('l', strtotime($response['end']['local']));
    $calendar = explode('-', $event_date);
    $whole_date = date('F j, Y', strtotime($response['start']['local']));
    $end_whole_date = date('F j, Y', strtotime($response['end']['local']));
    $tz = new DateTimeZone($response['start']['timezone']);
    $get_timezone = new DateTime($response['start']['local'],$tz);
    $time_zone_string = $get_timezone->format('T');
    $calendar_date = $calendar[2];
    $calendar_month = $calendar[1];
    $city = $response['venue']['address']['city'];
    if($city == 'New York') {
        $bg_color = "#fff";
    } else {
        $bg_color = "#f7f7f7";
    }
    $date_msg = '';
    if($whole_date == $end_whole_date){
        $date_msg = $day.', '.$whole_date.' at '.$start_event_time.' - '.$end_event_time;
    } else {
        $date_msg = $day.', '.$whole_date.' at '.$start_event_time.' - '.$end_day.', '.$end_whole_date.' at '.$end_event_time;
    }
    $html = str_get_html($desc_response['description']);
?>
<div class="event-banner">
    <div class="event-intro">
        <h2 style="font-weight: bold;"><?php echo $response['name']['text']; ?></h2>
        <?php
            if($response['online_event'] == true){
        ?>
        <h5 style="font-weight: bold;" class="calendar-month">Online Event</h5>
        <?php
            } else {
        ?>
        <h5 style="font-weight: bold;" class="calendar-month"><?php echo $response['venue']['address']['region']; ?></h5>
        <!--<h5 style="font-weight: bold;" class="calendar-month"><?php echo $day.', '.$whole_date; ?></h5>-->
        <h5 style="font-weight: bold;" class="address"><?php echo $response['venue']['name']; ?></h5>
        <?php
            echo ($response['venue']['address']['localized_area_display'] != null) ? '<h5 style="font-weight: bold;" class="address">'.$response['venue']['address']['localized_address_display'].'</h5>':'<h5 style="font-weight: bold;" class="address"></h5>';
            }
        ?>
        <h5 style="font-weight: bold;" class="calendar-month"><?php echo $date_msg. ' ('.$time_zone_string.')'; ?></h5>
        <button type="button" class="btn rsvp_button event_details_rsvp" data-event_id = "<?php echo $response['id']; ?>" data-calendar_day = "<?php echo $calendar_date; ?>" data-calendar_month = "<?php echo $calendar_month; ?>">RSVP <i class="fa fa-angle-double-right"></i></button>
    </div>
    <div class="text-center smooth-scroll d-block d-md-none"><a class="btn-scroll-mobile" href="#event-content"><img src="<?php echo base_url('assets/images/scroll.png'); ?>"/></a></div>
    <div class="text-center smooth-scroll d-none d-md-block"><a class="btn-scroll" href="#event-content"><img src="<?php echo base_url('assets/images/scroll.png'); ?>"/></a></div>
</div>
<div style="background:#f7f7f7">
    <div class="col-md-12">
        <div class="twenty-spacer"></div>
        <h2 class="text-center" style="font-size:40px; font-weight: bold;">SPECIAL GUESTS</h2>
    </div>
<?php
    // Find all images
    $img_counter = count($html->find('img'));
    $div = '';
    if($img_counter == 1){
        $div = "<div class='col-md-12 text-center'>";
    } else if($img_counter > 1){
		if($img_counter%2){
			$div = "<div class='col-md-3 text-center'>";
		} else {
			$div = "<div class='col-md-6 text-center bimg'>";
		}
	}

    $add_before = '';
	$add_after = '';
    $sep_counter = 0;
	$test_arr = array();
	$new_test_arr = array();
	$combined_arr = array();
	$testctr = 0;
    if($event_id == '106012845274' || $event_id == '105529487538' || $event_id == '105091294892'){
        /**non-mobile */
        echo "<div class='container d-none d-md-block' style='width:80%;'>";
        echo "<div class='row'>";
        foreach($html->find('img') as $element){
            echo '<div class="col-sm text-center"><h4>'.$element->title.'</h4><img width="280px" height="280px" class="img-thumbnail img-responsive" src="'.$element->src.'"/></div>';
        }
        echo "</div>";
        echo "</div>";
        /**nmobile */
        echo "<div class='container d-block d-md-none'>";
        echo "<div class='row'>";
        foreach($html->find('img') as $element){
            echo '<div class="col-sm text-center" style="padding-top: 20px;"><h4>'.$element->title.'</h4><img width="280px" height="280px" class="img-thumbnail img-responsive" src="'.$element->src.'"/></div>';
        }
        echo "</div>";
        echo "</div>";
        if($html->find('span#special_guest')){
            echo '<div class="col-md-12"><div class="col-sm text-center" style="padding-top: 20px;"><h4>'.$html->find('span#special_guest')[0]->plaintext.'</h4></div></div>';
        }
    } else {
        foreach($html->find('div') AS $get_test){
			if($get_test->find('img')){
                if($get_test->nextSibling()){
                    if($get_test->nextSibling()->innertext){
                        $combined_arr[] = $get_test->nextSibling()->innertext;
                        $new_test_arr[] = $get_test->nextSibling()->find('p',0)->innertext;
                        $new_test_arr[] = $get_test->nextSibling()->find('p',1)->innertext;
                        $speaker_name = str_replace('Speaker Name: ', '', $get_test->nextSibling()->find('p',0)->innertext);
                        $position = str_replace('Position: ', '', $get_test->nextSibling()->find('p',1)->innertext);
                        $speaker_title = $speaker_name.', '.$position;
                        $test_arr[$testctr] = $speaker_title;
                        $testctr++;
                    }
                }
			}
        }
        if($img_counter > 1){
            //2 images
           /**non-mobile */
            echo "<div class='container d-none d-md-block' style='width:80%;'>";
            echo "<div class='row'>";
            foreach($html->find('img') as $element){
                echo '<div class="col-sm text-center"><h4>'.$test_arr[$sep_counter].'</h4><img width="280px" height="280px" class="img-thumbnail img-responsive" src="'.$element->src.'"/></div>';
                $sep_counter++;
            }
            echo "</div>";
            echo "</div>";
            /**nmobile */
            echo "<div class='container d-block d-md-none'>";
            echo "<div class='row'>";
            $sep_counter2 = 0;
            foreach($html->find('img') as $element){
                echo '<div class="col-sm text-center" style="padding-top: 20px;"><h4>'.$test_arr[$sep_counter2].'</h4><img width="280px" height="280px" class="img-thumbnail img-responsive" src="'.$element->src.'"/></div>';
                $sep_counter2++;
            }
            echo "</div>";
            echo "</div>";
        } else {
            //single image
            foreach($html->find('img') as $element){
                if($img_counter > 1){
                    if($sep_counter%2){
                        $add_before = '';
                        $add_after = '</div><div class="col-md-2"></div></div><div class="twenty-spacer"></div>';
                    } else {
                        $add_before = '<div class="row"><div class="col-md-2"></div><div  class="col-md-8">';
                        $add_after = '';
                    }
                } else {
                    $add_after = '<div class="twenty-spacer"></div>';
                }
                echo $add_before.$div.'<h4>'.$test_arr[$sep_counter].'</h4><img width="280px" height="280px" class="img-thumbnail img-responsive" src="'.$element->src.'"/></div>'.$add_after;
                $sep_counter++;
            }
        }
    }
?>
    <div class="col-md-12">
        <div class="twenty-spacer"></div>
    </div>
</div>
<div class="row event-banner">
    <div id="event-content" class="col-md-12" style="height:104px;"></div>
    <div class="col-md-8 offset-md-2 col-xs-10 offset-xs-1 event-desc">
        <?php 
            if($event_id == '106012845274' || $event_id == '105529487538' || $event_id == '105091294892'){
                function teaser( $html ) {
                    $html = str_replace( '&nbsp;', ' ', $html );
                    do {
                        $tmp = $html;
                        $html = preg_replace('#<([^ >]+)[^>]*>[[:space:]]*</\1>#', '', $html );
                    } while ( $html !== $tmp );

                    return $html;
                }
                $content = preg_replace("/<img[^>]+\>/i", "", $html);
                $content = preg_replace('/<span [^<]*?id="([^<]*?speaker_name.*?)">(.*?)<\/span>/','',$content);
                $content = preg_replace('/<span [^<]*?id="([^<]*?special_guest.*?)">(.*?)<\/span>/','',$content);
                $desc_html = str_get_html(teaser($content));
                $new_content = '';
                $summary = $desc_html->find("div", 0);
                if($summary){
                    $new_content = '<p style="text-align: center;"><span style="color: #000000;"><strong><span style="font-size: medium; font-family: tahoma, arial, helvetica, sans-serif;"><span style="font-size: large;">'.$summary->plaintext.'</span></strong></span></p>';
                }
                foreach($desc_html->find('p') AS $test){
                    $desc_content = preg_replace("/<img[^>]+\>/i", "", $test->plaintext);
                    if($test->innertext != '' || $test->innertext != '&nbsp;'){
                        $new_content .= '<p style="text-align: center;"><span style="color: #000000;"><strong><span style="font-size: medium; font-family: tahoma, arial, helvetica, sans-serif;"><span style="font-size: large;">'.$test->plaintext.'</span></strong></span></p>';
                    }
                }
            } else {
                $new_content = '';
                function teaser( $html ) {
                    $html = str_replace( '&nbsp;', ' ', $html );
                    do {
                        $tmp = $html;
                        $html = preg_replace('#<([^ >]+)[^>]*>[[:space:]]*</\1>#', '', $html );
                    } while ( $html !== $tmp );

                    return $html;
                }
                $content = preg_replace("/<img[^>]+\>/i", "", $html);
                $desc_html = str_get_html(teaser($content));
                
                    $summary = $desc_html->find("div", 0);
                    if($summary){
                        $new_content = '<p style="text-align: center;"><span style="color: #000000;"><strong><span style="font-size: medium; font-family: tahoma, arial, helvetica, sans-serif;"><span style="font-size: large;">'.$summary->plaintext.'</span></strong></span></p>';
                    }
                foreach($desc_html->find('p') AS $test){
                    if($test->innertext != '' || $test->innertext != '&nbsp;'){
                        if(!in_array($test->plaintext,$new_test_arr)){
                            $new_content .= '<p style="text-align: center;"><span style="color: #000000;"><strong><span style="font-size: medium; font-family: tahoma, arial, helvetica, sans-serif;"><span style="font-size: large;">'.$test->plaintext.'</span></strong></span></p>';
                        }
                    }
                }
            }
            echo $new_content;
        ?>
    </div>
    <div class="col-md-12 fifty-spacer"></div>
</div>
<div class="row d-none d-md-block" style="background:#f7f7f7">
	<div class="col-md-12 thirty-spacer"></div>
	<div class="col-md-12 table-response">
		<table class="table" id="as_featured_in">
			<tbody>
				<tr>
                    <td><h1>As featured in</h1></td>
					<td><img src="<?php echo base_url('assets/images/cnbc.png'); ?>" /></td> <!--CNBC-->
					<td><img src="<?php echo base_url('assets/images/forbes.png'); ?>" /></td> <!--FORBES-->
					<td><img src="<?php echo base_url('assets/images/fortune.png'); ?>" /></td> <!--FORTUNE-->
					<td><img src="<?php echo base_url('assets/images/inc.png'); ?>" /></td> <!--INC-->
					<td><img src="<?php echo base_url('assets/images/TE.png'); ?>" /></td> <!--TE-->
					<td><img src="<?php echo base_url('assets/images/business_insider.png'); ?>" /></td> <!--BUSINESS INSIDER-->
				</tr>
			</tbody>
		</table>
	</div>
    <div class="col-md-12 thirty-spacer"></div>
	</div>
</div>
<div class="row d-block d-md-none" style="background:#f7f7f7">
	<div class="col-md-12 thirty-spacer"></div>
	<div class="col-xs-12 text-center"><h1>As featured in</h1></div>
	<div class="col-xs-12 text-center" style="padding:15px;"><img src="<?php echo base_url('assets/images/cnbc.png'); ?>" /></div><!--CNBC-->
	<div class="col-xs-12 text-center" style="padding:15px;"><img src="<?php echo base_url('assets/images/forbes.png'); ?>" /></div><!--FORBES-->
	<div class="col-xs-12 text-center" style="padding:15px;"><img src="<?php echo base_url('assets/images/fortune.png'); ?>" /></div><!--FORTUNE-->
	<div class="col-xs-12 text-center" style="padding:15px;"><img src="<?php echo base_url('assets/images/inc.png'); ?>" /></div><!--INC-->
	<div class="col-xs-12 text-center" style="padding:15px;"><img src="<?php echo base_url('assets/images/TE.png'); ?>" /></div><!--TE-->
    <div class="col-xs-12 text-center" style="padding:15px;"><img src="<?php echo base_url('assets/images/business_insider.png'); ?>" /></div><!--BUSINESS INSIDER-->
</div>
<div class="row" style="background: #fff;">
    <div class="col-md-12 col-xs-12">
        <section class="col-md-10 offset-md-1 text-center event-details">
            <h1 style="font-size: 45px; font-weight: bold;">EVENT TIMELINE</h1>
            <div class="col-md-12 col-xs-12">
                <div class="d-none d-md-block">
                    <img style="width:80%;" src="<?php echo base_url('assets/images/timeline_graphic.png'); ?>" />
                </div>
                <div class="d-block d-md-none">
                    <img style="width:100%;" src="<?php echo base_url('assets/images/timeline_graphic.png'); ?>" />
                </div>
            </div>
        </section>
    </div>
</div>
<div class="row" style="background: #fff;">
    <div class="col-md-12">
        <section class="col-md-10 offset-md-1 text-center event-details">
            <h1 style="font-size: 45px; font-weight: bold;">WELCOME TO BRUNCHWORK</h1>
            <div class="d-none d-md-block">
                <div class="fifty-spacer"></div>
                <h1 style="font-family: 'Crimson Text' !important; font-style: italic; line-height: 1.3em;font-size: 45px; font-weight: bold;">A selective community for up-and-coming executives, entrepreneurs and game-changers. Find the access, resources, and community you need to get ahead.</h1>
            </div>
            <div class="d-block d-md-none">
                <div class="twenty-spacer"></div>
                <h2 style="font-family: 'Crimson Text' !important; font-style: italic; line-height: 1.3em;font-weight: bold;">A selective community for up-and-coming executives, entrepreneurs and game-changers. Find the access, resources, and community you need to get ahead.</h2>
            </div>
        </section>
        <div class="d-none d-md-block">
            <script>
                $(document).ready(function(){
                    var slider = $('#content-slider').lightSlider({
                        loop:true,
                        keyPress:true,
                        speed: 4000, //ms'
                        loop: true,
                        slideEndAnimation: true,
                        pause: 5000,
                    });
                    slider.play();
                });

            </script>
            <section class="col-md-12 text-center event-details">
                <ul id="content-slider" class="content-slider">
                    <li>
                        <img src="<?php echo base_url('assets/images/1.jpg'); ?>" />
                    </li>
                    <li>
                        <img src="<?php echo base_url('assets/images/2.jpg'); ?>" />
                    </li>
                    <li>
                        <img src="<?php echo base_url('assets/images/3.jpg'); ?>" />
                    </li>
                    <li>
                        <img src="<?php echo base_url('assets/images/4.jpg'); ?>" />
                    </li>
                    <li>
                        <img src="<?php echo base_url('assets/images/5.jpg'); ?>" />
                    </li>
                    <li>
                        <img src="<?php echo base_url('assets/images/6.jpg'); ?>" />
                    </li>
                    <li>
                        <img src="<?php echo base_url('assets/images/7.jpg'); ?>" />
                    </li>
                </ul>
            </section>
        </div>
        <div class="d-block d-md-none"><!--Mobile-->
            <script>
                $(document).ready(function(){
                    var slider = $('#content-slider-mobile').lightSlider({
                        item: 1,
                        loop:true,
                        keyPress:true,
                        speed: 4000, //ms'
                        loop: true,
                        slideEndAnimation: true,
                        pause: 5000,
                    });
                    slider.play();
                });
            </script>
             <section class="col-md-12 text-center event-details">
                <ul id="content-slider-mobile" class="content-slider">
                    <li>
                        <img src="<?php echo base_url('assets/images/1.jpg'); ?>" />
                    </li>
                    <li>
                        <img src="<?php echo base_url('assets/images/2.jpg'); ?>" />
                    </li>
                    <li>
                        <img src="<?php echo base_url('assets/images/3.jpg'); ?>" />
                    </li>
                    <li>
                        <img src="<?php echo base_url('assets/images/4.jpg'); ?>" />
                    </li>
                    <li>
                        <img src="<?php echo base_url('assets/images/5.jpg'); ?>" />
                    </li>
                    <li>
                        <img src="<?php echo base_url('assets/images/6.jpg'); ?>" />
                    </li>
                    <li>
                        <img src="<?php echo base_url('assets/images/7.jpg'); ?>" />
                    </li>
                </ul>
            </section>
        </div>
    </div>
</div>
<?php
	if($city == 'New York') {
?>
<div class="row" style="background:#f7f7f7;">
    <section class="col-md-8 offset-md-2 col-xs-12 text-center event-details" style="padding:bottom: 0;">
        <h1 class="wellb col-xs-12" style="font-size: 45px; font-weight: bold;">WEEKEND CLUBHOUSE</h1>
        <div class="thirty-spacer"></div>
        <p class="text-center" style="font-size: 18px; color: #000;">Powered by:</p>
        <a href="https://www.industriousoffice.com/" rel="noopener" target="_blank"><img class="img-responsive" src="<?php echo base_url('assets/images/industrious.png'); ?>" width="300" ></a>
        <div class="thirty-spacer"></div>
        <div class="col-md-12 col-xs-12">
            <p class="text-center" style="line-height: 1.3em;font-size: 23px;">Gain access to industry leaders, business classes and socials at our chic weekend clubhouse in NY’s Bryant Park.</p>
        </div>
        <div class="twenty-spacer"></div>
        <ul id="clubhouse-slider" class="d-none d-md-block">
            <li>
                <img src="<?php echo base_url('assets/images/clubhouse/CH-1.jpg'); ?>" />
            </li>
            <li>
                <img src="<?php echo base_url('assets/images/clubhouse/CH-2.jpg'); ?>" />
            </li>
            <li>
                <img src="<?php echo base_url('assets/images/clubhouse/CH-3.jpg'); ?>" />
            </li>
            <li>
                <img src="<?php echo base_url('assets/images/clubhouse/CH-4.jpg'); ?>" />
            </li>
        </ul>
        <ul id="clubhouse-slider-mobile" class="d-block d-md-none">
            <li>
                <img src="<?php echo base_url('assets/images/clubhouse/CH-1-mobile.jpg'); ?>" />
            </li>
            <li>
                <img src="<?php echo base_url('assets/images/clubhouse/CH-2-mobile.jpg'); ?>" />
            </li>
            <li>
                <img src="<?php echo base_url('assets/images/clubhouse/CH-3-mobile.jpg'); ?>" />
            </li>
            <li>
                <img src="<?php echo base_url('assets/images/clubhouse/CH-4-mobile.jpg'); ?>" />
            </li>
        </ul>
    </section>
    <div class="fifty-spacer"></div>
</div>
<script>
    $(document).ready(function(){
        var clubhouseslider = $('#clubhouse-slider').lightSlider({
            item: 1,
            loop:true,
            keyPress:true,
            speed: 4000, //ms'
            loop: true,
            slideEndAnimation: true,
            pause: 5000
        });
        clubhouseslider.play(); 

        var clubhouseslider_mobile = $('#clubhouse-slider-mobile').lightSlider({
            item: 1,
            loop:true,
            keyPress:true,
            speed: 4000, //ms'
            loop: true,
            slideEndAnimation: true,
            pause: 5000
        });
        clubhouseslider_mobile.play(); 
    });

</script>
<?php		
	}
?>
<!-- OFFICIAL PARTNERS TEMPLATE
<?php
if($event_id == "74236077105") { ?>
<div class="col-md-12 col-xs-12 text-center" style="background-color: #f7f7f7;">
    <div class="fifty-spacer"></div>
    <h2 class="text-center" style="font-size:40px; font-weight: bold;">OFFICIAL PARTNERS</h2>
    <div class="fifty-spacer"></div>
    <a target="_blank" href="https://www.womeninmusic.org/"><img class="img-responsive" src="<?php echo base_url('assets/images/women_in_music.png'); ?>" alt="" ></a>
    <div class="fifty-spacer"></div>
</div>
<?php } ?>
-->
<div style="background-color: <?php echo $bg_color; ?>;">
    <div class="d-block d-md-none">
        <div class="fifty-spacer"></div>
        <div class="col-md-12 text-center">
            <h1 style="font-size: 45px; font-weight: bold;">WE ARE YOUR SECRET CAREER WEAPON</h1>
        </div>
    </div>
    <div class="d-none d-md-block">
        <div class="fifty-spacer"></div>
        <div class="twenty-spacer"></div>
        <div class="col-md-12 text-center">
            <h1 style="font-size: 45px; font-weight: bold;">WE ARE YOUR SECRET CAREER WEAPON</h1>
        </div>
    </div>
    <div class="d-none d-md-block">
        <div class="container" style="width: 70%;">
            <div class="row">
                <div class="col-sm text-center">
                    <div class="p-4">
                        <img class="img-responsive" src="<?php echo base_url('assets/images/creative_new.png'); ?>"/>
                    </div>
                    <h4 style="font-weight: bold">Tactical breakouts</h4>
                    <p class="secret_weapon">Get real time feedback on your business ideas</p>
                </div>
                <div class="col-sm text-center">
                    <div class="p-4">
                            <img class="img-responsive" src="<?php echo base_url('assets/images/collaboration_new.png'); ?>"/>
                        </div>
                        <h4 style="font-weight: bold">Meaningful connections</h4>
                        <p class="secret_weapon">Curated introductions that meet your goals</p>
                    </div>
                <div class="col-sm text-center">
                    <div class="p-4">
                            <img class="img-responsive" src="<?php echo base_url('assets/images/hand-shake_new.png'); ?>"/>
                        </div>
                        <h4 style="font-weight: bold">Warm and welcoming</h4>
                        <p class="secret_weapon">Most guests come solo and bond during the breakouts</p>
                    </div>
                </div>
        </div>
    </div>
    <div class="d-block d-md-none">
        <div class="container">
            <div class="row">
                <div class="col-sm text-center">
                    <div class="p-4">
                        <img class="img-responsive" src="<?php echo base_url('assets/images/creative_new.png'); ?>"/>
                    </div>
                    <h4 style="font-weight: bold">Tactical breakouts</h4>
                    <p class="secret_weapon">Get real time feedback on your business ideas</p>
                </div>
                <div class="col-sm text-center">
                    <div class="p-4">
                            <img class="img-responsive" src="<?php echo base_url('assets/images/collaboration_new.png'); ?>"/>
                        </div>
                        <h4 style="font-weight: bold">Meaningful connections</h4>
                        <p class="secret_weapon">Curated introductions that meet your goals</p>
                    </div>
                <div class="col-sm text-center">
                    <div class="p-4">
                            <img class="img-responsive" src="<?php echo base_url('assets/images/hand-shake_new.png'); ?>"/>
                        </div>
                        <h4 style="font-weight: bold">Warm and welcoming</h4>
                        <p class="secret_weapon">Most guests come solo and bond during the breakouts</p>
                    </div>
                </div>
            </div>
    </div>
    <div class="thirty-spacer"></div>
    <div class="row">
        <div class="col-sm text-center">
            <a class="btn btn-how-it-works fancybox-media" href="https://www.youtube.com/watch?v=JUM5SatlHfA&autoplay=1&rel=0&controls=0&showinfo=0"><b><i class="far fa-play-circle"></i> How It Works</b></a>
        </div>
    </div>
    <div class="fifty-spacer"></div>
    <div class="row" style="background:#f7f7f7">
        <div class="col-sm text-center nominate">
            <div class="fifty-spacer"></div>
            <span style="font-size: 22px;font-weight: bold;">Get a 15% promo code<span style="color: #ffa500;">*</span> when you <a href="https://brunchwork.com/referral-promotion" target="_blank">nominate 3 friends</a> to brunchwork</span>
            <div class="ten-spacer"></div>
            <div class="val"><span style="color:#ffa500">*</span> Valid for 1 ticket or 1 month of membership</div>
        </div>
    </div>
    <div class="fifty-spacer" style="background:#f7f7f7"></div>
</div>

<div style="background-color: #fff;">
    <div class="fifty-spacer"></div>
    <div class="col-md-8 offset-md-2 col-xs-12 text-center">
        <h1 style="color: #000; font-weight: bold;">LET'S STAY IN TOUCH</h1>
            <div id="containerforSubscribe" class=""></div>
            <script type="text/javascript">
                jQuery(function ($) { 
                    if(!jQuery('#wp-sponsors').attr('id')){
                        jQuery('#sponsor-eventpage').hide();
                    }
	                jQuery('#containerforSubscribe').bw_mailChimpSubscribeModal('bwSubs',{ susbscribetext:' Subscribe ' });
                }); 
            </script>
            <p class="join-community">JOIN OUR NEWSLETTER</p>
    </div>
</div>

<script>
    $(document).ready(function(){
        $('.loadingDiv').hide();
        $('.fancybox-media').fancybox({
            openEffect  : 'none',
            closeEffect : 'none',
            helpers : {
                media : {}
            }
        });

        $('a.btn-scroll, a.btn-scroll-mobile').click(function() {
            var sectionTo = $(this).attr('href');
            $('html, body').animate({
            scrollTop: $(sectionTo).offset().top
            }, 1500);
        });
    });

</script>
<div class="d-block d-md-none"><!--mobile-->
    <nav id="event_details_footer" class="navbar navbar-expand-lg" style="background: #222; margin-bottom:0;">
        <div class="col-sm text-center" id="navbarText" style="padding:0;">
            <ul class="list-inline">
                <li class="list-inline-item"><a class="nav-link no-decor" href="https://www.facebook.com/brunchwork" target="_blank"><i class="fab fa-facebook-f fa-2x"></i></a></li>
                <li class="list-inline-item"><a class="nav-link no-decor" href="https://www.twitter.com/brunchwork" target="_blank"><i class="fab fa-twitter fa-2x"></i></a></li>
                <li class="list-inline-item"><a class="nav-link no-decor" href="https://www.instagram.com/brunchwork" target="_blank"><i class="fab fa-instagram fa-2x"></i></a></li>
            </ul>
            <ul class="list-inline">
                <li class="list-inline-item"><a href="https://brunchwork.com/calendar" target="_blank">EVENTS</a></li>
                <li class="list-inline-item"><a href="https://brunchwork.com/newsletter1/">NEWSLETTER</a></li>
                <li class="list-inline-item"><a href="https://brunchwork.com/wisdom" target="_blank">WISDOM</a></li>
                <li class="list-inline-item"><a href="https://brunchwork.com/contact-us/" target="_blank">CONTACT US</a></li>
                <li class="list-inline-item"><a href="https://brunchwork.com/terms-and-policy-notice/" target="_blank">T&P</a></li>
            </ul>
            <span class="navbar-text" id="footer-info">
                &copy; 2018-<?php echo date("Y"); ?> <a href="https://brunchwork.com" target="_blank" id="bw_foot_a">brunchwork</a>. All rights reserved.
            </span>
        </div>
        
    </nav>
</div>
<div class="d-none d-md-block"><!--desktop-->
    <nav id="event_details_footer" class="navbar navbar-expand-lg" style="background: #222; margin-bottom:0;">
        <a class="navbar-brand" href="#"></a>
        <button class="navbar-toggler navbar-dark" type="button" data-toggle="collapse" data-target="#navbarText" aria-controls="navbarText" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse col-md-8 offset-md-2 col-xs-12" id="navbarText">
            <span class="navbar-text" id="footer-info">
                &copy; 2018-<?php echo date("Y"); ?> <a href="https://brunchwork.com" target="_blank" id="bw_foot_a">brunchwork</a>. All rights reserved.
            </span>
            <ul class="navbar-nav ml-auto list-inline text-center d-flex justify-content-center align-items-center">
                <li class="nav-item">
                    <a class="nav-link no-decor" href="https://www.facebook.com/brunchwork" target="_blank"><i class="fab fa-facebook-f fa-2x"></i></a>
                </li>
                <li class="nav-item">
                    <a class="nav-link no-decor" href="https://www.twitter.com/brunchwork" target="_blank"><i class="fab fa-twitter fa-2x"></i></a>
                </li>
                <li class="nav-item">
                <a class="nav-link no-decor" href="https://www.instagram.com/brunchwork" target="_blank"><i class="fab fa-instagram fa-2x"></i></a>
                </li>
                <li class="nav-item"><a href="https://brunchwork.com/calendar" target="_blank">EVENTS</a></li>
                <li class="nav-item"><a href="https://brunchwork.com/newsletter1/">NEWSLETTER</a></li>
                <li class="nav-item"><a href="https://brunchwork.com/wisdom" target="_blank">WISDOM</a></li>
                <li class="nav-item"><a href="https://brunchwork.com/contact-us/" target="_blank">CONTACT US</a></li>
                <li class="nav-item"><a href="https://brunchwork.com/terms-and-policy-notice/" target="_blank">T&P</a></li>
            </ul>
        </div>
    </nav>
</div>
<div class="center-parent loadingDiv"><div class="center-container"></div></div>
<script src="<?php echo base_url('assets/js/bootbox.min.js?v=').VER_NO; ?>"></script>