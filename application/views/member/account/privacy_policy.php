<div class="twenty-spacer"></div>
<h1>Privacy Policy</h1>
<div class="card-deck">
	<div class="card col-md-8">
		<div class="card-body">
			<p class="card-text"><small class="text-muted">Last Revised: November 1, 2015</small></p>
			<p>brunchwork (“brunchwork” or “we” or “us”) respects your privacy, and protecting your Personal Information is important to us. For the purposes of this Privacy Policy (“Policy”), “Personal Information” refers to any information that, alone or in connection with other information, may be able to identify you. This Policy covers the collection and use of information, including Personal Information, in connection with this Website (“Site”). By using this Site, you agree to the terms, conditions, and practices described in this Policy and our Terms of Service.</p>

			<strong>I. Information We Collect</strong>
			<p>When you register on our Site, sign up for email updates, or contact us, you provide us with Personal Information such as your name and email address. We also collect any information you voluntarily provide to us while using our Site or in connection with our services such as any information contained within messages you send through the Site. We may automatically receive information from your use of this Site including your IP address, browser information, and the date and time of your visit, and the pages you visit on our Site.</p>

			<strong>II. How We Use the Information</strong>
			<p>We use the information we collect in various ways such as responding to your requests, improving our Site and services, and communicating with you.</p>

			<strong>III. Information We Share with Others</strong>
			<p>We may share your information with website hosting partners and other parties who assist us in operating our website, conducting our business, processing payments, or providing our services (“Other Parties”).</p>

			<p>We may share or transfer your information when we believe such action is necessary to comply with the law, enforce our Site policies, or to protect our rights, property, and/or safety or that of you or Other Parties.</p>

			<p>Non-personally identifiable visitor information may be provided to other parties for marketing, advertising, or other uses. These parties will be unable to identify you based on this anonymous information.</p>

			<strong>IV. Third Party Links</strong>
			<p>On this Site, we may include links to third party websites which contain separate and independent privacy policies. We are not responsible for the practices of, or content contained within, these third parties websites, and recommend that you read and review the privacy policy of any such sites that you visit.</p>

			<strong>V. Cookies and Other Tracking Technologies</strong>
			<p>In order to improve this Site, brunchwork uses cookies. Cookies are small text files transferred to your computer’s hard drive through your web browser. Cookies enable the Site’s or service provider’s systems to recognize you browser and provide us with information about how our website is used.</p>

			<p>You may opt-out of receiving cookies from the use of this Site by following the instructions in your web browser, although this may interfere with your ability to fully use and enjoy our Site.</p>

			<strong>VI. Children’s Privacy</strong>
			<p>This Site is not intended for children under the age of 13 and request that children under 13 do not provide Personal Information through this Site. If we become aware that such information has been submitted through our Site, we will delete it from our database.</p>

			<strong>VII. Choices and Access</strong>
			<p>You may always choose not to provide information on our Site even though this may interfere with your ability to take advantage of this Site and our services.</p>

			<p>If you wish to review, access, update, or correct the personal information you have provided to us, you may do so by accessing your profile or contacting us

			<p>If you no longer wish to receive email communications from us, you may follow the instructions in our emails.</p>

			<strong>VIII. Users Outside the United States</strong>
			<p>This Site is hosted in the United States and is governed by United States law. If you are using this Site outside the United States, please be aware that your information will be transferred to, stored, and processed in the United States where our servers and databases are located. By using this Site, you consent to the transfer of information to countries outside your country of residence.</p>

			<strong>IX. Changes to this Policy</strong>
			<p>We are continually improving and adding to the features and functionality of this Site. As a result of these changes, or changes in the law, we may need to update or revise this Policy. Accordingly, brunchwork reserves the right to update or modify this Policy at any time by posting the revised version of the Policy on this webpage. We will notify you of any material changes to this policy by placing a notification on our homepage.</p>
		</div>
	</div>
	<div class="col-md-4"></div>
</div>
<div class="hundred-spacer"></div>