<link href="<?php echo base_url('assets/css/card-js.min.css?v='.VER_NO); ?>" rel="stylesheet">
<script src="https://js.braintreegateway.com/web/dropin/1.13.0/js/dropin.min.js"></script>
<script src="https://js.braintreegateway.com/web/3.38.0/js/client.min.js"></script>
<div class="twenty-spacer"></div>
<?php
	$user = $this->ion_auth->user()->row();
	$formatted_date = '';
	if($user->first_bill_date){
		$myDateTime = DateTime::createFromFormat('Y-m-d', $user->first_bill_date);
		$formatted_date = $myDateTime->format('F j, Y');
	}
    //print_r($user);
?>
<h1>Settings</h1>
<div class="padding-topbottom container">
	<div class="row">
		<div class="col-md-12 default_alert">
			<?php
				if($user->is_paused == 1){
			?>
			<div class="alert alert-warning" role="alert">
			<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
			Your member account is currently paused. If you wish to continue your membership, please contact support.
			</div>
			<?php		
				}
			?>
		</div>
		<div class="col-md-12 membership_alert">
		<?php 
			if($user->is_paused == 1 && $no_of_month > 0){
		?>
			<div class="alert alert-warning one-time-alert" role="alert">
			<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
			You have successfully paused your membership for <?php echo ($no_of_month > 1) ? $no_of_month.' months' : $no_of_month.' month'; ?>.
			</div>
		<?php
			} else if ($user->is_paused == 0 && $user->is_paused != NULL && $unpause == 'success') {
		?>
			<div class="alert alert-success one-time-alert" role="alert">
			<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
			Congratulations! You have successfully continued with your membership.
			</div>
		<?php
			}
		?>
		</div>
	</div>
</div>
<div class="card-deck" style="min-height: 191px;">
	<div class="card col-md-8 no-radius">
		<div class="card-body pb-0">
			<h5 class="card-title">Plan Details</h5>
			<hr />
			<table class="table borderless">
				<thead>
					<tr class="row">
						<th class="col">Member Since</th>
						<th class="col">Plan Type</th>
						<th class="col">Payment Term</th>
						<!-- <th class="col">Renewal Date</th> -->
					</tr>
				</thead>
				<tbody>
					<tr class="row">
						<td class="col"><?php echo $rsvp_date; ?></td>
						<td class="col"><?php echo $subscriber_plan; ?></td>
						<td class="col"><?php echo $payment_term; ?></td>
						<!-- <td class="col"><?php echo $renewal_date; ?></td> -->
					</tr>
				</tbody>
			</table>
			<table class="table borderless" style="display: none;">
				<tbody>
					<tr class="row">
						<th class="col">Guest passes remaining</th>
						<th class="col"><span style="color:red">*</span><?php echo $guest_pass; ?> <span style="font-size:11px;font-weight:normal;">We are double checking guest pass numbers so this maybe slightly off. This number will be updated shortly.</span></th>
					</tr>
				</tbody>
			</table>
		</div>
	</div>
	<div class="card col-md-4 no-radius buttons-card" style="min-height: 197px;">
		<div class="list-group list-group-flush">
			<a href="#" id="sidebarCollapse" class="list-group-item list-group-item-action"><i class="far fa-user-circle"></i> Edit Profile</a>
			<a class="list-group-item list-group-item-action contact-support-form add-pointer"><i class="far fa-question-circle"></i> Support</a>
			<a class="list-group-item list-group-item-action contact-support-form add-pointer"><i class="far fa-pause-circle"></i> Pause/Cancel</a>
			<a class="list-group-item list-group-item-action" href="https://brunchwork.com/member-guidelines/" target="_blank"><i class="far fa-list-alt"></i> Member Guidelines</a>
			<?php
			/*
				if($user->id == 13){
			?>
			<?php
				if(strtolower($user->is_paused) == 1){
			?>
			<a class="list-group-item list-group-item-action unpause-membership add-pointer"><i class="fa fa-play"></i> Continue with Membership</a>
			<?php
				} else {
			?>
			<a class="list-group-item list-group-item-action pause-membership add-pointer"><i class="fa fa-pause"></i> Pause Membership</a>
			<?php
				}
			?>
			<!--<a class="list-group-item list-group-item-action cancel-membership add-pointer"><i class="fa fa-times"></i> Cancel Membership</a>-->
			<?php
				}
			*/
			?>
		</div>
	</div>
</div>
<div class="fifty-spacer"></div>
<div class="card-deck">
	<div class="card col-md-8 no-radius">
		<div class="card-body">
			<h5 class="card-title">Card Information</h5>
			<hr />
			<div class="cc_alert"></div>
			<!--
			<div id="dropin-container"></div>
			<div class="text-center"><button type="button" class="btn btn-brunchwork btn-lg" id="submit-button">Add/Update Card</button></div>
			<script>
				var button = document.querySelector('#submit-button');

				braintree.dropin.create({
				authorization: '<?php echo $client_token; ?>',
				container: '#dropin-container',
				defaultFirst: true,
				vaultManager: true
				}, function (createErr, instance) {
				button.addEventListener('click', function () {
					instance.requestPaymentMethod(function (err, payload) {
					// Submit payload.nonce to your server
					});
				});
				});
			</script>
			-->
			
			<form id="credit_card_form">
				<div class="card-js"></div>
				<div class="twenty-spacer"></div>
				<?php
					$csrf = array(
							'name' => $this->security->get_csrf_token_name(),
							'hash' => $this->security->get_csrf_hash()
						);
				?>
				<input type="hidden" name="<?=$csrf['name'];?>" value="<?=$csrf['hash'];?>" />
				<div class="text-center"><button type="button" class="btn btn-brunchwork btn-lg" id="credit_card_btn">Update Card</button></div>
			</form>
			
		</div>
	</div>
	<div class="card col-md-4"  style="height: 250px !important;"><!--margin-top: -5%; -->
		<p></p><p></p>
		<h3 >Reward yourself.</h3>
		
		<ul>
			<li>Get <strong>15% off</strong> your next month of membership, when you add brunchwork to your LinkedIn.</li>
			<li>Get <strong>50% off</strong> your next month of membership, when one of your friends submits a member application.</li>
		</ul>
	</div>
</div>
<div class="fifty-spacer"></div>
<div class="card-deck">
	<div class="card col-md-8 no-radius">
		<div class="card-body">
			<h5 class="card-title">Account Email</h5>
			<hr />
			<div class="update_email_alert"></div>
			<form id="email_form">
			<input type="email" class="form-control no-radius" name="update_email" value="<?php echo $email_address; ?>" />
			<div class="twenty-spacer"></div>
			<?php
				$csrf = array(
						'name' => $this->security->get_csrf_token_name(),
						'hash' => $this->security->get_csrf_hash()
					);
			?>
			<input type="hidden" name="<?=$csrf['name'];?>" value="<?=$csrf['hash'];?>" />
			<div class="text-center"><button type="button" class="btn btn-brunchwork btn-lg" id="email_btn">Update Email</button></div>
			</form>
		</div>
	</div>
	<div class="card col-md-4 empty bg-pink"></div>
</div>

<div class="fifty-spacer"></div>

<div class="card-deck">
	<div class="card col-md-8 no-radius">
		<div class="card-body">
			<h5 class="card-title">Recent Invoice History</h5>
			<hr />
			<table class="table borderless">
				<thead>
					<tr class="row">
						<th class="col">ID</th>
						<th class="col">Payment Date</th>
						<th class="col">Amount</th>
						<th class="col">Payment Status</th>
					</tr>
				</thead>
				<tbody>
					<?php 
						if(!$transactions || $transaction_count == 0){
							$creditcard_token = '';
					?>
					<tr class="row">
						<td width="100%" colspan="4" class="text-center">No data found.</td>
					</tr>
					<?php
					} else {
						$max_count = 12;
						$counter = 0;
						if($transaction_count > 0){
						foreach($transactions AS $get_transactions){
							if($counter == 0){
							}
							if($cc_bin == $get_transactions->creditCard['bin']){
								$creditcard_token = $get_transactions->creditCard['token'];
							}
							if($counter < $max_count){
								$currency = $get_transactions->currencyIsoCode;
								$payment_id = $get_transactions->id;
								$payment_date = $get_transactions->createdAt->format('F j, Y');
								$payment_amount = $get_transactions->amount;
								$payment_status = $get_transactions->status;

						
					?>
					<tr class="row">
						<td class="col"><?php echo $payment_id; ?></td>
						<td class="col"><?php echo $payment_date; ?></td>
						<td class="col"><?php echo number_format($payment_amount, 2).' '.$currency; ?></td>
						<td class="col"><?php echo ucfirst($payment_status); ?></td>
					</tr>
					<?php	
							}
							$counter++;
						}
					} 
					} ?>
				</tbody>
			</table>
		</div>
	</div>
	<div class="card col-md-4 empty bg-pink"></div>
</div>
<?php
	echo "<input type='hidden' name='cc_token' id='cc_token' value='".$token."' />";
	echo "<input type='hidden' name='cc_bin' id='cc_bin' value='".$cc_bin."' />";
	echo "<input type='hidden' name='customer_braintree_id' id='customer_braintree_id' value='".$customer_braintree_id."' />";
?>
<div class="hundred-spacer"></div>
<div class="center-parent loadingDiv"><div class="center-container"></div></div>
<script type="text/javascript" src="<?php echo base_url('assets/js/card-js.min.js?V='.VER_NO);?>"></script>
<script type="text/javascript" src="<?php echo base_url('assets/js/settings.js?V='.VER_NO);?>"></script>
<script>
	var is_paused = '<?php echo $user->is_paused; ?>';
    var first_bill_date = '<?php echo $user->first_bill_date; ?>';
</script>
<script src="<?php echo base_url('assets/js/bootbox.min.js?v=').VER_NO; ?>"></script>
<script src="<?php echo base_url('assets/js/membership.js?v=').VER_NO; ?>"></script>