
<?php
$this->load->view('templates/memberportal_header');
?>
<div class="container">
<?php if($user == "super_admin"){ ?>
    <?php if($list_type == "past") { ?>
    <a href="<?php echo base_url('admin'); ?>"><span style="float:right"> &nbsp; &nbsp; Upcoming Events</span></a>
    <?php } elseif ($list_type == "upcoming") { ?>
        <a href="<?php echo base_url('admin-past-events'); ?>"><span style="float:right"> &nbsp; &nbsp; Past Events</span></a>
    <?php } ?>
<?php } ?>

<a href="<?php echo base_url('logout'); ?>"><span style="float:right"> &nbsp; &nbsp; Logout</span></a>

<div class="admin_h1"><?php if($list_type == "past" && $user == "super_admin") { ?>  &nbsp; &nbsp; Past Events <?php } elseif($list_type == "upcoming"){?>  &nbsp; &nbsp; Upcoming Events <?php }?> </div>

<div id="main_data"></div>
<div class="center-parent loadingDiv" ><div class="center-container"></div></div>
      <!-- Modal -->
<div id="guests_dis_Modal" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        
        <h4 class="modal-title">Guests</h4>
      </div>
      <div class="modal-body">
        <table class="table">
            <thead>
                <tr>
                    <th>Guest Name</th>
                    <?php if($user == "super_admin"){ ?> <th>Guest Email</th> <?php } ?>
                </tr>
            </thead>
            <tbody id="guest_body">
                
            </tbody>
        </table>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>

  </div>
</div>      

</div>

<script type="text/javascript">
var user_type = '<?php echo $user; ?>';
var list_type = '<?php echo $list_type; ?>';

$('#main_data').html("");
$.ajax({
    url: window.base_url + 'admin/load_data',
    type: 'get',
    dataType: 'html',
    data: {'list_type': list_type},
    success: function(response){
        console.log(response);
        $('#main_data').html(response);
        $('.loadingDiv').css('display', 'none');
    }
});

function get_guests(eventid, memberemail){
        $('#guest_body').html("");
        $.ajax({
        url: window.base_url + 'admin/get_guests',
        type: 'get',
        data: {'eventid': eventid, 'member_email': memberemail},
        dataType: 'json',
        success: function(response){
            if(response.length < 1){
                $('#guest_body').html("No guests invited!");
            }
            for (var i = response.length - 1; i >= 0; i--) {
                if(user_type == 'super_admin'){
                    var guest_body_html = "<tr><td>"+response[i]['guest_first_name']+" "+response[i]['guest_last_name']+"</td><td>"+response[i]['guest_email']+"</td></tr>";
                } else {
                    var guest_body_html = "<tr><td>"+response[i]['guest_first_name']+" "+response[i]['guest_last_name']+"</td></tr>";
                }
                $('#guest_body').append(guest_body_html);
            }
        }
    });
}

$(document).on('click', 'button.download_guest', function(){
    var event_id = $(this).data('event_id');
    window.open('<?php echo base_url("admin/admin/download_guests?event_id="); ?>' + event_id);
});
var loaded = false;
var loaded_events = [];
$(document).on('click', '.btn-event-name', function(){
    //console.log(loaded_events);
    var eventid = $(this).data('event_id');
    $('.table_guests').html();
    if ( $(this).attr('aria-expanded') == 'true' ) {
    } else {

        if(jQuery.inArray( eventid, loaded_events ) != -1){
        }else{
            $('.yy').append('<div class="center-parent loadingDiv"><div class="center-container"></div></div>');
            $.ajax({
                url: window.base_url + 'admin/get_attendees_event',
                type: 'get',
                data: {'eventid': eventid},
                dataType: 'json',
                retrieve: true,
                success: function(response){   
                    //console.log(response);      
                    var x = new Array();
                    if(response.length < 1){
                       
                    }
                    
                    for (var i = response.length - 1; i >= 0; i--) {
                        if(user_type == 'super_admin'){
                            x[i] = new Array(response[i]['attendee_first_name']+"<span class='' style='display: none;'>-"+response[i]['member_has_guests']+"</span>", response[i]['attendee_last_name'], response[i]['ticket_class_name'],response[i]['attendee_email'],response[i]['company'], response[i]['position'],response[i]['member_first_bill_data'], response[i]['from']);
                        } else {
                           x[i] = new Array(response[i]['attendee_first_name']+"<span class='' style='display: none;'>-"+response[i]['member_has_guests']+"</span>", response[i]['attendee_last_name'], response[i]['ticket_class_name']+"<span class='email_for_admin' style='display: none;'>-"+response[i]['attendee_email']+"</span>",response[i]['company'], response[i]['position'],response[i]['member_first_bill_data'], response[i]['from']);
                        }
                    }
                    $('.loadingDiv').hide();
                    if(user_type == 'super_admin'){
                        var table_columns = [
                            { title: "Member Firstname" },
                            { title: "Member Surname" },
                            { title: "Ticket Type" },
                            { title: "Member Email" },
                            { title: "Company" },
                            { title: "Position" },
                            { title: "RSVP Date" },
                            { title: "From" }
                        ];
                    } else {
                        var table_columns = [
                            { title: "Member Firstname" },
                            { title: "Member Surname" },
                            { title: "Ticket Type" },
                            
                            { title: "Company" },
                            { title: "Position" },
                            { title: "RSVP Date" },
                            { title: "From" }
                        ];
                    }
                    var table = $('.member-guests-table'+eventid).DataTable( {
               
                        data: x,
                        destroy: true,
                        columns: table_columns,
                        "responsive": true,
                         dom: 'Bfrtip',
                        "columnDefs": [
                            {
                                "targets": [ -1 ],
                                "orderable": false, //set not orderable
                            }
                        ],
                        buttons: [
                            { 
                                "extend": 'excelHtml5', 
                                "text":'Excel',
                                "className": 'btn btn-success', 
                                "titleAttr": 'Export to Excel',
                                "exportOptions": { 
                                    columns: ':not(:last-child)', 
                                    format: {
                                        body: function ( data, row, column, node ) {
                                            // Strip $ from salary column to make it numeric
                                            if(column === 0){
                                                data = data.replace(/<.*?>/g, "");
                                                data = data.replace(/(-undefined|-true|-false)/g, "");
                                                return $.trim(data);
                                            } else if(column === 2){
                                                data = data.replace(/<.*?>/g, "");
                                                data = data.split('-');
                                                return data[0];
                                            }else {
                                                return data;
                                            }
                                        }
                                    }
                                } 
                            },
                            { 
                                "extend": 'pdfHtml5', 
                                "text":'PDF',
                                "className": 'btn btn-primary', 
                                "titleAttr": 'Export to PDF',
                                "exportOptions": { 
                                    columns: ':not(:last-child)', 
                                    format: {
                                        body: function ( data, row, column, node ) {
                                            // Strip $ from salary column to make it numeric
                                            if(column === 0){
                                                data = data.replace(/<.*?>/g, "");
                                                data = data.replace(/(-undefined|-true|-false)/g, "");
                                                return $.trim(data);
                                            } else if(column === 2){
                                                data = data.replace(/<.*?>/g, "");
                                                data = data.split('-');
                                                return data[0];
                                            }else {
                                                return data;
                                            }
                                        }
                                    }
                                }
                            },
                            { 
                                "extend": 'copyHtml5', 
                                "text":'Copy',
                                "className": 'btn btn-info', 
                                "titleAttr": 'Copy',
                                "exportOptions": { 
                                    columns: ':not(:last-child)', 
                                    format: {
                                        body: function ( data, row, column, node ) {
                                            // Strip $ from salary column to make it numeric
                                            if(column === 0){
                                                data = data.replace(/<.*?>/g, "");
                                                data = data.replace(/(-undefined|-true|-false)/g, "");
                                                return $.trim(data);
                                            } else if(column === 2){
                                                data = data.replace(/<.*?>/g, "");
                                                data = data.split('-');
                                                return data[0];
                                            }else {
                                                return data;
                                            }
                                        }
                                    }
                                }
                            },
                            { 
                                "extend": 'print', 
                                "text":'Print',
                                "className": 'btn btn-warning', 
                                "titleAttr": 'Print', 
                                "exportOptions": { 
                                    columns: ':not(:last-child)', 
                                    format: {
                                        body: function ( data, row, column, node ) {
                                            // Strip $ from salary column to make it numeric
                                            if(column === 0){
                                                data = data.replace(/<.*?>/g, "");
                                                data = data.replace(/(-undefined|-true|-false)/g, "");
                                                return $.trim(data);
                                            } else if(column === 2){
                                                data = data.replace(/<.*?>/g, "");
                                                data = data.split('-');
                                                return data[0];
                                            }else {
                                                return data;
                                            }
                                        }
                                    }
                                }
                            }
                            //'copy', 'csv', 'excel', 'pdf', 'print'
                        ],
                        "createdRow": function ( row, data, index ) {
                            var sad = data[0].split("-");
                            var mem_emaild = sad[1];
                            var sada = mem_emaild.split("</span>");
                            if(sada[0] == "true"){
                                $('td', row).addClass('highlight');
                            }
                        }
                    } );
                    loaded = true;
                    loaded_events.push(eventid);
                    $('.mgt tbody').on('click', 'tr', function () {
                        //var data = table.row( this ).data();
                        
                        var thisp = $(this);
                        if(user_type == 'super_admin'){
                            var mem_email = $(this).children('td:nth-child(4)').text();
                        }
                        else{
                            var get_mem_email = $(this).children('td:nth-child(3)').text();
                            //console.log(get_mem_email);
                            var sd = get_mem_email.split("-");
                           // console.log(sd[1]);
                            var mem_email = sd[1];

                        }
                       // alert( $(this).children('td:nth-child(4)').text() );
                        
                        $.ajax({
                            url: window.base_url + 'admin/get_guests',
                            type: 'get',
                            data: {'eventid': eventid, 'member_email': mem_email},
                            dataType: 'json',
                            success: function(response){
                               var breaks = "<br/><br/><br/>";
                                for (var i = response.length - 1; i >= 0; i--) {
                                    
                                    thisp.children('td:nth-child(1)').append(breaks+"<span class='table_guests'>"+response[i]['guest_first_name']+"</span>");
                                    thisp.children('td:nth-child(2)').append(breaks+"<span class='table_guests'>"+response[i]['guest_last_name']+"</span>");
                                    if(user_type == 'super_admin'){
                                     thisp.children('td:nth-child(4)').append(breaks+"<span class='table_guests'>"+response[i]['guest_email']+"</span>");
                                    }
                                    if(user_type == 'admin'){
                                        $('.email_for_admin').append(breaks+"<span class='table_guests'>"+response[i]['guest_last_name']+"</span>");
                                    }
                                    breaks = "<br/>";
                                }
                                
                            }
                            
                        });
                    } );
                }
            });
        }
    }  
});

</script>
<?php
$this->load->view('templates/memberportal_footer');
?>