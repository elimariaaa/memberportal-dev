<?php
defined('BASEPATH') OR exit('No direct script access allowed');
 
class Guestpass extends CI_Controller {
 
    function __construct()
    {
        parent::__construct();
        $this->load->library('ion_auth');
        $this->load->model('GuestpassModel');
        $this->load->library("braintree_lib");
    }
    
    public function index()
    {
        //get all active 
        $all_active = $this->GuestpassModel->get_active_users();
        $this->load->library('email');
        $this->email->from('noreply@brunchwork.com', 'brunchwork Member Portal');
        $this->email->to('marren26@gmail.com');
        $this->email->subject('brunchwork Member Portal - Update Guest Pass');
        $this->email->message($all_active);
        if ( ! $this->email->send())
        {
            echo 'Email not sent. '.$this->email->print_debugger();
        } else {
            echo 'Email sent. '.date('Y-m-d H:i:s');
        }
    }

    function update_users(){
        $this->load->model('UserModel');
        $users = $this->UserModel->get_users_with_missing_data();
        if($users){
            foreach($users AS $user_data):
                if($user_data['braintree_subscription_id'] != ''){
                    $subscription = $this->braintree_lib->find_subscription($user_data['braintree_subscription_id']);
                    /*
                    echo $user_data['braintree_subscription_id'];
                    echo "<br />";
                    */
                    $first_billing_date = $subscription->firstBillingDate->format('Y-m-d');
                    $plan_id = $subscription->planId;
                    $status = $subscription->status;
                    $update_arr = array('first_bill_date' => $first_billing_date, 'braintree_plan_id' => $plan_id, 'braintree_subscription_status' => $status);
                    $update_data = $this->UserModel->update_users_with_missing_data($user_data['braintree_subscription_id'], $update_arr);
                }
            endforeach;
        }
    }
}