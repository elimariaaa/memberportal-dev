<?php
defined('BASEPATH') OR exit('No direct script access allowed');
error_reporting(0);
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;
/*require_once './vendor/autoload.php';*/

class Admin extends CI_Controller {

    function __construct()
    {
        parent::__construct();
        $this->load->library('ion_auth');
        $this->load->model('UserModel');
        $this->load->model('EventsModel');
        $this->load->library("braintree_lib");
        $this->check_admin();

    }
    function check_admin(){
        if (!$this->ion_auth->is_admin() && !$this->is_super_admin())
        {   
          $this->session->set_flashdata('message', 'You must be an admin to view this page');
            redirect('/dashboard');
        }
        

    }
    public function index_old(){
        if($this->is_super_admin()){
            $user_type = "super_admin";
        }
        if($this->ion_auth->is_admin()){
            $user_type = "admin";
        }
        
        $rsvp_data = $this->UserModel->get_all_rsvp();
        $event_ids = array();
        $all_event_ids = array();

        $all_event_ids = $this->get_all_events();
        //echo "<pre>"; print_r($rsvp_data); echo "</pre>"; exit;
        foreach ($all_event_ids as $event_id_index => $event_id) {
            # code...
            $event_details = $this->get_event_data($event_id);
            //$event_details = $this->get_event_data($value['event_id']);
            $event_date = date('Y-M-d', strtotime($event_details['start']['local']));
            $start_event_time = date('g:i A', strtotime($event_details['start']['local']));
            $end_event_time = date('g:i A', strtotime($event_details['end']['local']));
            $day = date('l', strtotime($event_details['start']['local']));
            $calendar = explode('-', $event_date);
            $whole_date = date('F j, Y', strtotime($event_details['start']['local']));
            $calendar_date = $calendar[2];
            $calendar_month = $calendar[1];
            //$eventbrite_event_details = $this->get_event_attendes($event_id);
            foreach ($rsvp_data as $value) {
                //if($value['event_id'] == $event_id){
                    $member_first_name  = $value['member_first_name'];
                    $member_last_name  = $value['member_last_name'];
                    $member_email  = $value['member_email'];
                    $member_data = $this->UserModel->check_braintree_status($member_email);
                    $member_first_bill_data = '';
                    if($member_data){
                        //get earliest RSVP date
                        $rsvp_date = $this->UserModel->get_min_rsvp($member_data->id);
                        $member_first_bill_data = ( $rsvp_date && $rsvp_date->rsvp_date != NULL) ? date('Y-m-d', strtotime($rsvp_date->rsvp_date)) : '';
                    }
                    /*
                    $member_first_bill_data = '';
                    if($member_data){
                        $member_first_bill_data = ($member_data->first_bill_date != '') ? $member_data->first_bill_date:'';
                    }
                    */
                    if($event_details['shareable'] == true){
                        $event_ids[$event_id][]  = [
                            "member_first_name" => $member_first_name, //$value['member_first_name'],
                            "member_last_name" => $member_last_name, //$value['member_last_name'],
                            "member_email" => $member_email, //$value['member_email'],
                            "member_first_bill_data" => $member_first_bill_data,
                            "event_name" =>  $event_details['name']['text'],
                            "event_date" => $calendar_date,
                            "event_month" => $calendar_month,
                            "event_city" => ($event_details['venue'] != null) ? $event_details['venue']['address']['city'] : '',
                            "event_id" => $event_id,
                            "from" => 'Portal',
                            "guests" => $this->UserModel->get_all_guests($event_id)
                        ];
                    }
               // }
            }
        }
        
        $data = ["data" => $event_ids, 'pagetitle' => 'brunchwork | Admin', "user" => $user_type,  "list_type" => "upcoming"];
        $this->load->view('admin/admin', $data);
    }

    public function index(){
        if($this->is_super_admin()){
            $user_type = "super_admin";
        }
        if($this->ion_auth->is_admin()){
            $user_type = "admin";
        }
        
        
        
        $data = [ 'pagetitle' => 'brunchwork | Admin', "user" => $user_type,  "list_type" => "upcoming"];
        $this->load->view('admin/admin', $data);
    }

    public function load_data(){
        $list_type = $_GET['list_type'];
        $rsvp_data = $this->UserModel->get_all_rsvp();
        $event_ids = array();
        $all_event_ids = array();
        if($list_type == "upcoming"){

            $all_event_ids = $this->get_all_events();
            foreach ($all_event_ids as $event_details) {
                //$event_details = $this->get_event_data($value['event_id']);
                $event_date = date('Y-M-d', strtotime($event_details['start']['local']));
                $start_event_time = date('g:i A', strtotime($event_details['start']['local']));
                $end_event_time = date('g:i A', strtotime($event_details['end']['local']));
                $day = date('l', strtotime($event_details['start']['local']));
                $calendar = explode('-', $event_date);
                $whole_date = date('F j, Y', strtotime($event_details['start']['local']));
                $calendar_date = $calendar[2];
                $calendar_month = $calendar[1];
                //$eventbrite_event_details = $this->get_event_attendes($event_id);
                foreach ($rsvp_data as $value) {
                    //if($value['event_id'] == $event_id){
                    $member_first_name  = $value['member_first_name'];
                    $member_last_name  = $value['member_last_name'];
                    $member_email  = $value['member_email'];
                    /*$member_data = $this->UserModel->check_braintree_status($member_email);
                    $member_first_bill_data = '';
                    if($member_data){
                        //get earliest RSVP date
                        $rsvp_date = $this->UserModel->get_min_rsvp($member_data->id);
                        $member_first_bill_data = ( $rsvp_date && $rsvp_date->rsvp_date != NULL) ? date('Y-m-d', strtotime($rsvp_date->rsvp_date)) : '';
                    }*/

                    if($event_details['shareable'] == true){
                        $event_ids[$event_details['id']][]  = [
                            "member_first_name" => $member_first_name, //$value['member_first_name'],
                            "member_last_name" => $member_last_name, //$value['member_last_name'],
                            "member_email" => $member_email, //$value['member_email'],
                            //"member_first_bill_data" => $member_first_bill_data,
                            "event_name" =>  $event_details['name']['text'],
                            "event_date" => $calendar_date,
                            "event_month" => $calendar_month,
                            "event_city" => ($event_details['venue'] != null) ? $event_details['venue']['address']['city'] : '',
                            "event_id" => $event_details['id'],
                            "from" => 'Portal',
                            "guests" => $this->UserModel->get_all_guests($event_details['id'])
                        ];
                    }
                    // }
                }


            }
        }


        else if($list_type == "past"){
            $past_events_data = $this->get_past_event_data();

            foreach ($past_events_data as $past_event) {
                $past_event_data[] = [
                    "event_id" => $past_event['event_id'],
                    "event_name" => $past_event['event_name'],
                    "event_date" => $past_event['event_date'],
                    "event_month" => $past_event['event_month'],
                    "event_city" => $past_event['event_city'],
                ];
            }

            //  print_r($past_event_data); exit;

            foreach ($past_event_data as $eID) {
                $event_ids[$eID['event_id']][]  = [
                    "event_name" =>  $eID['event_name'],
                    "event_date" => $eID['event_date'],
                    "event_month" => $eID['event_month'],
                    "event_city" => $eID['event_city'],
                    "event_id" => $eID['event_id'],
                    "guests" => $this->UserModel->get_all_guests($eID['event_id'])
                ];
            }
        }

        $html_data = "";

        foreach ($event_ids as  $key => $value) {
            //print_r($list_type); exit;
            if($list_type == "past") {
                $event_name = isset($value['event_name']) ? $value['event_name'] : $value[0]['event_name'];
                $event_month = isset($value['event_month']) ? $value['event_month'] : $value[0]['event_month'];
                $event_date = isset($value['event_date']) ? $value['event_date'] : $value[0]['event_date'];
                $event_city = isset($value['event_city']) ? $value['event_city'] : $value[0]['event_city'];
                //$member_first_bill_data = isset($value['member_first_bill_data']) ? $value['member_first_bill_data'] : $value[0]['member_first_bill_data'];
                $guests = $value[0]['guests'];
                if($guests > 1){
                    $no_of_guests = $guests.' Guests';
                } else {
                    $no_of_guests = $guests.' Guest';
                }
                $event_id = $value[0]['event_id'];
            } elseif($list_type == "upcoming"){
                //echo "<pre>"; print_r($value); echo "</pre>"; die();
                $event_name = isset($value[0]['event_name']) ? $value[0]['event_name'] : $value[0]['event_name'];
                $event_month = isset($value[0]['event_month']) ? $value[0]['event_month'] : $value[0]['event_month'];
                $event_date = isset($value[0]['event_date']) ? $value[0]['event_date'] : $value[0]['event_date'];
                $event_city = isset($value[0]['event_city']) ? $value[0]['event_city'] : $value[0]['event_city'];
                //$member_first_bill_data = isset($value[0]['member_first_bill_data']) ? $value[0]['member_first_bill_data'] : $value[0]['member_first_bill_data'];
                $guests = $value[0]['guests'];
                if($guests > 1){
                    $no_of_guests = $guests.' Guests';
                } else {
                    $no_of_guests = $guests.' Guest';
                }
                $event_id = $value[0]['event_id'];
            }


            $html_data .= '
            <button type="button" class="btn btn-custom event_button btn-event-name" data-toggle="collapse" data-target="#demo'.$key.'" data-event_id="'.$event_id.'"><strong> <span style="color: #000;">'.$event_name.'</span></strong> || '.$event_month.' '.$event_date.' || '.$event_city.'</button>
            <div id="demo'.$key.'" class="collapse">
                <div class="col-md-12"><button class="btn btn-brunchwork download_guest guest-count-"'.$event_id.'" data-event_id="'.$event_name.'" disabled="">'.$no_of_guests.'</button></div>

                
                <div class="yy table-responsive">
                    <table class="table mgt member-guests-table'.$event_id.'">
                       
                        
                    </table> 
                </div>
            </div>';

        }
        echo ($html_data);
    }

    public function get_guests(){
        $event_id = $this->input->get('eventid');
        $member_email = $this->input->get('member_email');

       
        $guests_data = $this->UserModel->get_guests($member_email, $event_id);

        echo json_encode($guests_data);
        
    }

    public function get_event_data($eventid){
        $csrf = array(
                'name' => $this->security->get_csrf_token_name(),
                'hash' => $this->security->get_csrf_hash()
            );
           
        //$token = "5JVTZDONWCOPI2WUIWST";
        //$organizer_id =  "7806405987";
        //$request_url = "https://www.eventbriteapi.com/v3/events/search/?organizer.id=".$organizer_id."&token=".$token;
        $request_url = "https://www.eventbriteapi.com/v3/events/".$eventid."?token=".EVENT_TOKEN."&expand=venue";
        //$params = array('sort_by' => 'date', 'organizer.id' => $organizer_id, 'token' => $token);
        $context = stream_context_create(
                        array(
                            'http' => array(
                                'method'  => 'GET',
                                'header'  => "Content-type: application/x-www-form-urlencoded\r\n"//,
                                //'content' => http_build_query($params)
                            )
                        )
                    );
        $json_data = file_get_contents( $request_url, false, $context );
        //$response = get_object_vars(json_decode($json_data));
        $response = json_decode($json_data, true);
        //$get_count = $response['pagination']['object_count'];
        return $response;
    }

    public function get_event_attendes($event_id){
        $csrf = array(
                'name' => $this->security->get_csrf_token_name(),
                'hash' => $this->security->get_csrf_hash()
            );
           $response2 = array();
        //$token = "5JVTZDONWCOPI2WUIWST";
        //$organizer_id =  "7806405987";
        //$request_url = "https://www.eventbriteapi.com/v3/events/search/?organizer.id=".$organizer_id."&token=".$token;
        $request_url = "https://www.eventbriteapi.com/v3/events/".$event_id."/attendees?token=".EVENT_TOKEN;
        //$params = array('sort_by' => 'date', 'organizer.id' => $organizer_id, 'token' => $token);
        $context = stream_context_create(
                        array(
                            'http' => array(
                                'method'  => 'GET',
                                'header'  => "Content-type: application/x-www-form-urlencoded\r\n"//,
                                //'content' => http_build_query($params)
                            )
                        )
                    );
        $json_data = file_get_contents( $request_url, false, $context );
        //$response = get_object_vars(json_decode($json_data));
        $response = json_decode($json_data, true);

        $page_count = $response["pagination"]["page_count"];

        if($page_count > 1){
           // $json_data2 = "";
            
           /* for ($i=0; $i <= (int)$page_count; $i++) { */
                $request_url2 = "https://www.eventbriteapi.com/v3/events/".$event_id."/attendees?token=".EVENT_TOKEN."&page=2";
        //$params = array('sort_by' => 'date', 'organizer.id' => $organizer_id, 'token' => $token);
        
        $json_data2 = file_get_contents( $request_url2, false, $context );
        //$response = get_object_vars(json_decode($json_data));
        $response2 = json_decode($json_data2, true);
        $merged_res =  array_merge_recursive($response, $response2);
            return $merged_res ;
        }
       
        return $response;
    }


    public function get_all_events(){
        $csrf = array(
                'name' => $this->security->get_csrf_token_name(),
                'hash' => $this->security->get_csrf_hash()
            );
        $formatted_date =  (new \DateTime('America/New_York'))->format('Y-m-d\T00:00:00');

        $request_url = "https://www.eventbriteapi.com/v3/organizations/".ORGANIZER_ID."/events/?status=live&token=".EVENT_TOKEN."&expand=venue";
        $context = stream_context_create(
                        array(
                            'http' => array(
                                'method'  => 'GET',
                                'header'  => "Content-type: application/x-www-form-urlencoded\r\n"//,
                                //'content' => http_build_query($params)
                            )
                        )
                    );
        $json_data = file_get_contents( $request_url, false, $context );
        //$response = get_object_vars(json_decode($json_data));
        $response = json_decode($json_data, true);
        //$get_count = $response['pagination']['object_count'];
        
        foreach ($response['events'] as $event) {
            $allevent_data[] = $event;
        }
        return $allevent_data;
    }


    public function import(){
        $data = ['pagetitle' => 'brunchwork | Admin - Import Users'];
        $this->load->view('admin/import', $data);
    }

    public function excel(){

        $reader = new \PhpOffice\PhpSpreadsheet\Reader\Xlsx();
        $file_mimes = array('application/vnd.ms-excel', 'application/excel', 'application/vnd.msexcel', 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
        if(isset($_FILES['upload_file']['name']) && in_array($_FILES['upload_file']['type'], $file_mimes)) {

            $arr_file = explode('.', $_FILES['upload_file']['name']);
            $extension = end($arr_file);
            $spreadsheet = $reader->load($_FILES['upload_file']['tmp_name']);

            $users_data= $spreadsheet->getActiveSheet()->toArray();
            unset($users_data[0]);

            //echo "<pre>"; print_r($users_data); //exit;

            foreach ($users_data as $row) {
                if($row[1] && $row[0]){
                    switch ($row[5]) {
                        case 'SF':
                            $city_id = 2;
                            break;
                        case 'NY':
                        case 'NYC':
                            $city_id = 1;
                            break;
                        case 'LA':
                            $city_id = 3;
                            break;
                        case 'BOS':
                            $city_id = 6;
                            break;
                        case 'Other':
                            $city_id = 7;
                            break;
                        case 'Virtual':
                            $city_id = 8;
                            break;
                        default:
                            $city_id = 7;
                            break;
                    }
                    
                    /*switch (trim(ucwords($row[8]))) {
                        case 'Samsung':
                        case 'Uber':
                        case 'Oracle':
                        case 'ADP':
                        case 'Github':
                        case 'Lyft':
                        case 'Google':
                        case 'LinkedIn':
                        case 'Amazon':
                            $industry = "Big Tech";
                            break;
                        case 'EY':
                            $industry = "Consulting";
                            break;
                    }*/
                    if(strpos(trim(ucwords($row[8])), "Amazon") !== false){
                         $industry = "Big Tech";
                    }

                    else if(strpos(trim(ucwords($row[8])), "Samsung") !== false){
                        $industry = "Big Tech";
                    }

                    else if(strpos(trim(ucwords($row[8])), "Uber") !== false){
                        $industry = "Big Tech";
                    }

                    else if(strpos(trim(ucwords($row[8])), "Oracle") !== false){
                        $industry = "Big Tech";
                    }

                    else if(strpos(trim(ucwords($row[8])), "ADP") !== false){
                        $industry = "Big Tech";
                    }

                    else if(strpos(trim(ucwords($row[8])), "Github") !== false){
                        $industry = "Big Tech";
                    }

                    else if(strpos(trim(ucwords($row[8])), "Lyft") !== false){
                        $industry = "Big Tech";
                    }

                    else if(strpos(trim(ucwords($row[8])), "LinkedIn") !== false){
                        $industry = "Big Tech";
                    }

                    else if(strpos(trim(ucwords($row[8])), "Google") !== false){
                        $industry = "Big Tech";
                    }

                    else if(strpos(trim(ucwords($row[8])), "EY") !== false){
                        $industry = "Consulting";
                    }

                    else{

                        switch (trim(ucwords($row[7]))) {
                        case 'Advertising':
                            $industry = "Marketing and Advertising";
                            break;
                        case 'Law / Legal':
                            $industry = "Law";
                            break;
                        case 'Law':
                            $industry = "Law";
                            break;
                        case 'Legal':
                            $industry = "Law";
                            break;
                        case 'Management':
                            $industry = "Consulting";
                            break;
                        case 'Consultant':
                            $industry = "Consulting";
                            break;
                        case 'Startups and Founders':
                            $industry = "Startups";
                            break;
                        case 'Founder':
                            $industry = "Startups";
                            break;
                        case 'Startup':
                            $industry = "Startups";
                            break;
                        case 'Fashion/Beauty':
                            $industry = "Fashion / Beauty";
                            break;
                        case 'Fashion':
                            $industry = "Fashion / Beauty";
                            break;
                        case 'Beauty':
                            $industry = "Fashion / Beauty";
                            break;
                        default:
                             $industry = trim(ucwords($row[7]));
                            break;
                    }
                    }

                    $data = array( 
                        'braintree_customer_id'  =>  $row[1], 
                        'braintree_subscription_id'  =>  $row[0], 
                        'first_name' =>  $row[2], 
                        'last_name'   =>  $row[3],
                        'username'   =>  $row[4],
                        'email'   =>  $row[4],
                        'city_id' => $city_id,
                        'industry' => $industry,
                        'company' => $row[8],
                        'position' => $row[9],
                        'profile_image' => $row[14],
                        'active' => 1,
                        'braintree_payment_term' => $row[11],
                        'alumni' => $row[17]
                    );
                    try{
                        $query = $this->db->get_where('ci_users', array('email' => $row[4]));

                        // if user exists, update
                        if($query->result()){

                            $this->db->update('ci_users', $data, array('email' => $row[4]));

                             $query_user_group = $this->db->get_where('ci_users_groups', array('user_id' => $query->result()[0]->id));

                            if($query_user_group->result()){}else{
                                $user_group_data  = array(

                                    'user_id' => $query->result()[0]->id,
                                    'group_id' => '2',

                                );
                                $this->db->insert('ci_users_groups', $user_group_data);
                            }


                        }else{
                            // insert new user
                            $this->db->insert('ci_users', $data);
                            $inserted_user_id = $this->db->insert_id();

                            $user_group_data  = array(

                                'user_id' => $inserted_user_id,
                                'group_id' => '2',

                            );
                            $this->db->insert('ci_users_groups', $user_group_data);
                        }

                    }catch(Exception $e){
                        // print_r("error");
                        // return;
                    }
                }
            }
            $base_url =  base_url('assets/css/');
            //print_r($base_url);
    
            echo "<p style='font-size: 2rem; text-align: center; margin-top: 15%; border: 1px solid orange; padding: 25px; margin: 15% auto; width: 50%;'>Upload success, redirecting to dashboard <img src='" .  $base_url . "fancybox_loading.gif'><p>";

            $url = base_url();  
            //header("Refresh: 5; url=$url");
            if( headers_sent() ) { echo("<script>setTimeout(function(){ location.href='$url' }, 5000);</script>"); }
                else { header("Refresh: 5; URL=$url"); }
        } else{
            echo "<p style='text-align: center; margin-top: 15%; border: 1px solid red; padding: 25px; margin: 0 auto; width: 50%;'>Error, Please try again</p>";
        }

    }

    public function import_payment_terms(){
        //importing payment_terms

        $payment_terms_file = "assets/payment_terms.xlsx";
        $spreadsheet = \PhpOffice\PhpSpreadsheet\IOFactory::load($payment_terms_file);
        $payment_terms_data = $spreadsheet->getActiveSheet()->toArray();
        unset($payment_terms_data[0]);
        
        foreach ($payment_terms_data as $key => $value) {
            $pt_data = ['braintree_payment_term' => $value[1]];
            $this->db->where('email', $value[0]);
            $this->db->update('ci_users',  $pt_data);
        }
        //echo "<pre>"; print_r($payment_terms_data); exit;
        redirect('/admin');
    }
    
    public function is_super_admin(){
        $user_details = $this->ion_auth->user()->row_array();
        $user_id = $user_details['id']; 
        $dads = $this->db->where('group_id', 3)
        ->where('user_id', $user_id)
                        ->get('ci_users_groups')->result_array();
        if(count($dads) == 1){
            return true;
        }
        else{
            return false;
        }
    }

    public function get_past_event_data(){
        $csrf = array(
                'name' => $this->security->get_csrf_token_name(),
                'hash' => $this->security->get_csrf_hash()
            );
           
        $request_url = "https://www.eventbriteapi.com/v3/organizations/".ORGANIZER_ID."/events/?status=ended&order_by=start_desc&expand=venue&token=".EVENT_TOKEN;

        $context = stream_context_create(
                        array(
                            'http' => array(
                                'method'  => 'GET',
                                'header'  => "Content-type: application/x-www-form-urlencoded\r\n"//,
                                //'content' => http_build_query($params)
                            )
                        )
                    );
        $json_data = file_get_contents( $request_url, false, $context );
        //$response = get_object_vars(json_decode($json_data));
        $response = json_decode($json_data, true);
        //$get_count = $response['pagination']['object_count'];
        
        foreach ($response['events'] as $event) {
           
            $event_date = date('Y-M-d', strtotime($event['start']['local']));
            $start_event_time = date('g:i A', strtotime($event['start']['local']));
            $end_event_time = date('g:i A', strtotime($event['end']['local']));
            $day = date('l', strtotime($event['start']['local']));
            $calendar = explode('-', $event_date);
            $whole_date = date('F j, Y', strtotime($event['start']['local']));
            $calendar_date = $calendar[2];
            $calendar_month = $calendar[1];
            $event_data[] = [
                "event_id" => $event['id'],
                "event_name" => $event['name']['text'],
                "event_city" => $event['venue']['address']['region'],
                "event_date" => $calendar_date,
                "event_month" => $calendar_month,
            ];
        }
        return $event_data;        
    }

    public function get_past_events(){
        $past_events_data = $this->get_past_event_data();

        foreach ($past_events_data as $past_event) {
            $past_event_data[] = [
                "event_id" => $past_event['event_id'],
                "event_name" => $past_event['event_name'],
                "event_date" => $past_event['event_date'],
                "event_month" => $past_event['event_month'],
                "event_city" => $past_event['event_city'],
            ];
        }

      //  print_r($past_event_data); exit;

        foreach ($past_event_data as $eID) {
            $event_ids[$eID['event_id']][]  = [
                    "event_name" =>  $eID['event_name'],
                    "event_date" => $eID['event_date'],
                    "event_month" => $eID['event_month'],
                    "event_city" => $eID['event_city'],
                    "event_id" => $eID['event_id'],
                    "guests" => $this->UserModel->get_all_guests($eID['event_id'])
                ];
        }

        if($this->is_super_admin()){
            $user_type = "super_admin";
        }
        if($this->ion_auth->is_admin()){
            $user_type = "admin";
        }
        //print_r($event_ids); exit;
        $data = ["data" => $event_ids, 'pagetitle' => 'brunchwork | Admin', "user" => $user_type, "list_type" => "past"];
        $this->load->view('admin/admin', $data);

        //echo "<pre>"; print_r($event_ids); echo "</pre>"; exit;
    }  
    
    public function get_attendees_event(){
        $event_id = $this->input->get('eventid');
        $eventbrite_event_details = $this->get_event_attendes($event_id);
        $rsvp_data = $this->UserModel->get_event_rsvp($event_id);
        $event_ids = [];
        foreach ($rsvp_data as $value) {
            $realtime_rsvp_date = $value['rsvp_date'];
            $member_first_name  = $value['member_first_name'];
            $member_last_name  = $value['member_last_name'];
            $company  = isset($value['company']) ? $value['company'] : " ";
            $position  = isset($value['position']) ? $value['position'] : " ";
            
            $member_first_bill_data = '';
            $member_data = $this->UserModel->check_braintree_status($value['member_email']);
            if($realtime_rsvp_date != null){
                $member_first_bill_data = date('Y-m-d', strtotime($realtime_rsvp_date));
            } else {
                if($member_data){
                    //get earliest RSVP date
                    $rsvp_date = $this->UserModel->get_min_rsvp($member_data->id);
                    $member_first_bill_data = ( $rsvp_date && $rsvp_date->rsvp_date != NULL) ? date('Y-m-d', strtotime($rsvp_date->rsvp_date)) : '';
                }
            }
            /*
            if($member_data){
                $member_first_bill_data = ($member_data->first_bill_date != '') ? $member_data->first_bill_date:'';
            }
            */
            if($this->ion_auth->in_group(3)){
                $member_email  = $value['member_email'];
                $event_ids[]  = [
                    "attendee_first_name" => $member_first_name, //$value['member_first_name'],
                    "attendee_last_name" => $member_last_name, //$value['member_last_name'],
                    "attendee_email" => $member_email, //$value['member_email'],
                    "member_has_guests" => $this->UserModel->member_has_guests($value['member_id'], $event_id),
                    "ticket_class_name" => "Members Only",
                    "event_id" => $event_id,
                    "from" => 'Portal',
                    "company" => $company,
                    "position" => $position,
                    "guests" => $this->UserModel->get_all_guests($event_id),
                    "member_first_bill_data" => $member_first_bill_data,
                    'rsvp_date' => date('Y-m-d', strtotime($rsvp_date))
                ];
            } else {
                $member_email  = $value['member_email'];
                $event_ids[]  = [
                    "attendee_first_name" => $member_first_name, //$value['member_first_name'],
                    "attendee_last_name" => $member_last_name, //$value['member_last_name'],
                    "ticket_class_name" => "Members Only",
                    "event_id" => $event_id,
                        "attendee_email" => $member_email,
                        "member_has_guests" => $this->UserModel->member_has_guests($value['member_id'], $event_id),
                    "from" => 'Portal',
                    "company" => $company,
                    "position" => $position,
                    "guests" => $this->UserModel->get_all_guests($event_id),
                    "member_first_bill_data" => $member_first_bill_data,
                    'rsvp_date' => date('Y-m-d', strtotime($rsvp_date))
                ];
            }
                
            
            
        }
            
        foreach ($eventbrite_event_details['attendees'] as $key => $attendee_details) {
            //print_r($attendee_details["answers"]);
            $attendee_company = isset($attendee_details["answers"][4]["answer"]) ? $attendee_details["answers"][4]["answer"] : " ";
            $attendee_position = isset($attendee_details["answers"][3]["answer"]) ? $attendee_details["answers"][3]["answer"] : " ";
            if(isset($attendee_details['profile']['email'])){
                $member_data = $this->UserModel->check_braintree_status($attendee_details['profile']['email']);
            } else {
                $member_data = '';
            }
            $member_first_bill_data = '';
            if($member_data){
                //get earliest RSVP date
                $rsvp_date = $this->UserModel->get_min_rsvp($member_data->id);
                $member_first_bill_data = ( $rsvp_date && $rsvp_date->rsvp_date != NULL) ? date('Y-m-d', strtotime($rsvp_date->rsvp_date)) : '';
            }
            /*
            $member_first_bill_data = '';
            if($member_data){
                $member_first_bill_data = ($member_data->first_bill_date != '') ? $member_data->first_bill_date:'';
            }
            */
                if($this->ion_auth->in_group(3)){                    
                    $event_ids[]  = [
                        "attendee_first_name" => $attendee_details['profile']['first_name'],
                        "attendee_last_name" => $attendee_details['profile']['last_name'],
                        "attendee_email" => isset($attendee_details['profile']['email']) ? $attendee_details['profile']['email']:'',
                        "member_first_bill_data" => date('Y-m-d', strtotime($attendee_details['created'])),
                        "ticket_class_name" => $attendee_details['ticket_class_name'],
                        "event_id" => $event_id,
                        "from" => 'Eventbrite',
                        "company" => $attendee_details['profile']['company'],
                        "position" => $attendee_details['profile']['job_title'],
                        "guests" => $this->UserModel->get_all_guests($event_id)
                    ];
                } else {
                    $event_ids[]  = [
                        "attendee_first_name" => $attendee_details['profile']['first_name'],
                        "attendee_last_name" => $attendee_details['profile']['last_name'],
                        "ticket_class_name" => $attendee_details['ticket_class_name'],
                        "attendee_email" => isset($attendee_details['profile']['email']) ? $attendee_details['profile']['email']:'',
                        "member_first_bill_data" => date('Y-m-d', strtotime($attendee_details['created'])),
                        "event_id" => $event_id,
                        "from" => 'Eventbrite',
                        "company" => $attendee_details['profile']['company'],
                        "position" => $attendee_details['profile']['job_title'],
                        "guests" => $this->UserModel->get_all_guests($event_id)
                    ];
                }
            }
            echo json_encode($event_ids);
    }

    function download_guests(){
        $event_id = $this->input->get('event_id');
        $all_guests = $this->UserModel->get_all_guests_data($event_id);
        $event_details = $this->get_event_data($event_id);
        $filename = str_replace(' ', '_', $event_details['name']['text']. " Guests.xls");
        
        header("Content-type: application/octet-stream");
        header("Content-Disposition: attachment; filename=$filename");
        header("Pragma: no-cache");
        header("Expires: 0");
        
        $columnHeader ='';
        if($this->ion_auth->in_group(3)){
            $columnHeader = " "."\t"."Guest First Name"."\t"."Guest Last Name"."\t"."Guest Email"."\t"."Date Added"."\t"."Member First Name"."\t"."Member Last Name"."\t"."Member Email"."\t"."Member Company"."\t"."Member Position"."\t";
        } else {
            $columnHeader = " "."\t"."Guest First Name"."\t"."Guest Last Name"."\t"."Date Added"."\t"."Member First Name"."\t"."Member Last Name"."\t"."Member Company"."\t"."Member Position"."\t";
        }
        $setData='';
        //$table = '<table border="1"><thead><tr><th></th><th>Guest First Name</th><th>Guest Last Name</th><th>Guest Email</th><th>Date Added</th><th>Member First Name</th><th>Member Last Name</th><th>Member Email</th></tr></thead>';
        //$table .= '<tbody>';
        $count = count($all_guests);
        $rowData = '';
        for($x = 0; $x < $count; $x++){
            $new_count = $x + 1;
            $rowData = '';
            $rowData .= '"' . $new_count . '"' . "\t";
            $rowData .= '"' . $all_guests[$x]['first_name'] . '"' . "\t";
            $rowData .= '"' . $all_guests[$x]['last_name'] . '"' . "\t";
            if($this->ion_auth->in_group(3)){
            $rowData .= '"' . $all_guests[$x]['email'] . '"' . "\t";
            }
            $rowData .= '"' . $all_guests[$x]['date_added'] . '"' . "\t";
            $rowData .= '"' . $all_guests[$x]['member_first_name'] . '"' . "\t";
            $rowData .= '"' . $all_guests[$x]['member_last_name'] . '"' . "\t";
            if($this->ion_auth->in_group(3)){
                $rowData .= '"' . $all_guests[$x]['member_email'] . '"' . "\t";
            }
            $rowData .= '"' . $all_guests[$x]['company'] . '"' . "\t";
            $rowData .= '"' . $all_guests[$x]['position'] . '"' . "\t";
            $setData .= trim($rowData)."\n";
        }
        
        echo ucwords($columnHeader)."\n".$setData."\n";
        //$table .= '</tbody></table>';
        //echo $table;
    }
}
