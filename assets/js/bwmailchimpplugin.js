 /*!
  * plugin for bw subscribe for mail chimp 
  * Author: @charanraj.tc
  * Licensed under the IISC license
  */

 ;
 (function($) {
     if (!$.bw) {
         $.bw = {};
     }

     $.bw.mailChimpSubscribeModal = function(el, uniqueIdentifier, options) {
         // To avoid scope issues, use 'base' instead of 'this'
         // to reference this class from internal events and functions.
         var base = this;
         // set the plugin name 
         base.pluginName = 'bw.mailChimpSubscribeModal';
         utHtml = '';
         // Access to jQuery and DOM versions of element
         base.$el = $(el);
         base.el = el;
         base.otherCityValue = 128;

         // Add a reverse reference to the DOM object
         base.$el.data("bw.mailChimpSubscribeModal", base);

         base.init = function() {
             base.uniqueIdentifier = uniqueIdentifier;

             base.options = $.extend({},
                 $.bw.mailChimpSubscribeModal.defaultOptions, options);

             // Put your initialization code here
             base.logMessage('initializing the plugin');
             if (typeof base.uniqueIdentifier !== 'string') {
                 base.logMessage('has no uniqueIdentifier or not a string ', base.uniqueIdentifier);
                 return;
             }

             base.outHtml = base.getOutHtmlWithUniqueIdentifier(base.uniqueIdentifier);

             base.logMessage('generated Output HTMl Form ');

             base.$el.html(base.outHtml);

             base.logMessage('added subscribe form to DOM ');

             base.linkEvents();

             base.logMessage('Reseting Form ');

             base.resetSubscribeForm();


         };

         base.traSubs = function() {

             var cityName = jQuery('option[value="' + base.inputFormData.city + '"]').first().text();
             if (base.inputFormData.city == base.otherCityValue) {
                 cityName = base.inputFormData.otherCity;
             }

             jQuery.ajax({
                 type: "POST",
                 url: "https://brunchwork.com/wp-admin/admin-ajax.php",
                 data: {
                     'action': 'ajax_mailchimpsubs_track',
                     'location': window.location.toString(),
                     'member_email': base.inputFormData.email,
                     'city': cityName,
                     'location_id': base.uniqueIdentifier
                 },
                 beforeSend: function() {
                     base.logMessage('track before');
                 },
                 success: function(res) {
                     base.logMessage('track success', res);
                 },
                 error: function() {
                     base.logMessage('track error');
                 },
             });
         };

         base.tracMeberships = function() {

             var cityName = jQuery('option[value="' + base.inputFormData.city + '"]').first().text();
             if (base.inputFormData.city == base.otherCityValue) {
                 cityName = base.inputFormData.otherCity;
             }


             jQuery.ajax({
                 type: "POST",
                 url: "https://brunchwork.com/wp-admin/admin-ajax.php",
                 data: {
                     'action': 'ajax_memebership_track',
                     'name': 'there',
                     'member_name': 'there',
                     'email': base.inputFormData.email,
                     'city': cityName,
                     'member_email': base.inputFormData.email
                 },
                 beforeSend: function() {
                     base.logMessage('track membership interest before');
                 },
                 success: function(res) {
                     base.logMessage('track membership interest success', res);
                 },
                 error: function() {
                     base.logMessage('track membership interest error');
                 },
             });
         };

         base.EmailRegExp = new RegExp("^[\w!#$%&'*+/=?`{|}~^-]+(?:\.[\w!#$%&'*+/=?`{|}~^-]+)*@(?:[A-Z0-9-]+\.)+[A-Z]{2,6}$");

         base.inputFormData = {
             email: '',
             city: '',
             citytext: '',
             otherCity: '',
         };


         base.getFormData = function() {
             base.logMessage('getting data from form');
             base.inputFormData.email = base.bwformEle.$emailInput.val();
             base.inputFormData.email = base.inputFormData.email.trim();

             base.inputFormData.city = base.bwformEle.$citySelect.val();
             base.inputFormData.city = base.inputFormData.city.trim();
             
             base.inputFormData.citytext = $('#' + base.bwformEle.citySelect +' option:selected').text();
             base.inputFormData.citytext = base.inputFormData.citytext.trim();

             base.inputFormData.otherCity = base.bwformEle.$otherCityInput.val();
             base.inputFormData.otherCity = base.inputFormData.otherCity.trim();
             base.logMessage('Form Data ', base.inputFormData);

         };


         base.validateSubscriberForm = function() {
             base.logMessage('Validating form');
             var results = [];
             // email 
             if (base.inputFormData.email === "") {
                 base.logMessage('Email is empty');
                 base.setEmailError('Your Email is Required ');

                 jQuery('#' + base.uniqueIdentifier + 'emailHelpBlock').removeClass('hide-error');
                 results.push(false);
             } else {
                 base.setEmailError('');
                 jQuery('#' + base.uniqueIdentifier + 'emailHelpBlock').addClass('hide-error');
                 results.push(true);
             }


             if (base.inputFormData.city === "") {
                 base.logMessage('city is empty');
                 base.setCityError('Please Select City ');
                 jQuery('#' + base.uniqueIdentifier + 'cityHelpBlock').removeClass('hide-error');
                 results.push(false);
             } else {
                 base.setCityError('');
                 jQuery('#' + base.uniqueIdentifier + 'cityHelpBlock').addClass('hide-error');
                 results.push(true);
             }

             if (base.inputFormData.city == base.otherCityValue && base.inputFormData.otherCity === "") {
                 base.logMessage('otherCity is empty');
                 base.setOtherCityError('Enter your City');
                 jQuery('#' + base.uniqueIdentifier + 'otherCityHelpBlock').removeClass('hide-error');
                 results.push(false);
             } else {
                 results.push(true);
                 base.setOtherCityError('');
                 jQuery('#' + base.uniqueIdentifier + 'otherCityHelpBlock').addClass('hide-error');
             }

             for (var i = 0; i <= results.length; i++) {
                 if (results[i] === false) {
                     return false;
                 }
             }
             return true;
         };

         base.isLoading = false;

         base.setLoading = function(loadingValue) {
             base.logMessage('set Loading ', loadingValue);
             if (loadingValue) {
                 base.bwformEle.$emailFromGroup.hide();
                 base.bwformEle.$cityFromGroup.hide();

                 if (base.inputFormData.city == base.otherCityValue) {
                     base.bwformEle.$otherCityFromGroup.hide();
                 }
                 base.bwformEle.$susbscribeButton.addClass('btn_subscribing');

                 base.bwformEle.$susbscribeButton.attr('disabled', 'disabled');


                 base.bwformEle.$susbscribeButton.html('Subscribing..');
             } else {
                 base.bwformEle.$susbscribeButton.removeClass('btn_subscribing');
                 base.bwformEle.$susbscribeButton.removeAttr('disabled');

                 base.bwformEle.$emailFromGroup.show();
                 base.bwformEle.$cityFromGroup.show();

                 if (base.inputFormData.city == base.otherCityValue) {
                     base.bwformEle.$otherCityFromGroup.show();
                 }

                 base.bwformEle.$susbscribeButton.html('Subscribe');
             }
         };

         base.getLoading = function() {
             base.logMessage('get Loading ', base.isLoading);
             return base.isLoading;
         };

         base.setEmailError = function(emailErrorMessage) {
             if (emailErrorMessage.trim() === '') {
                 base.bwformEle.$emailFromGroup.removeClass('has-error');
                 base.bwformEle.$emailHelpBlock.html('');
             } else {
                 base.bwformEle.$emailFromGroup.addClass('has-error');
                 base.bwformEle.$emailHelpBlock.html(emailErrorMessage);
             }
         };

         base.setCityError = function(cityErrorMessage) {
             if (cityErrorMessage.trim() === '') {
                 base.bwformEle.$cityFromGroup.removeClass('has-error');
                 base.bwformEle.$cityHelpBlock.html('');
             } else {
                 base.bwformEle.$cityFromGroup.addClass('has-error');
                 base.bwformEle.$cityHelpBlock.html(cityErrorMessage);
             }
         };

         base.setOtherCityError = function(otherCityErrorMessage) {
             if (otherCityErrorMessage.trim() === '') {
                 base.bwformEle.$otherCityFromGroup.removeClass('has-error');
                 base.bwformEle.$otherCityHelpBlock.html('');
             } else {
                 base.bwformEle.$otherCityFromGroup.addClass('has-error');
                 base.bwformEle.$otherCityHelpBlock.html(otherCityErrorMessage);
             }
         };
         base.responseBlockError = function(responseBlockErrorMessage) {
             if (responseBlockErrorMessage.trim() === '') {
                 base.bwformEle.$responseBlock.html('');
                 base.bwformEle.$responseBlockFromGroup.removeClass('has-error');
             } else {
                 base.bwformEle.$responseBlockFromGroup.addClass('has-error');
                 base.bwformEle.$responseBlock.html(responseBlockErrorMessage);
             }
         };
         base.responseBlockSuccess = function(responseBlockSuccessMessage) {
             if (responseBlockSuccessMessage.trim() === '') {
                 base.bwformEle.$responseBlock.html('');


             } else {
                 base.bwformEle.$responseBlock.html(responseBlockSuccessMessage);
                 jQuery('#' + base.uniqueIdentifier + 'responseHelpBlock').removeClass('hide-error');
             }
         };

         base.callMailchimpAPI = function() {
             // do ajax call for post request 
             var postFormData = {
                 'u': '1eac2f55934c399ba5a4201ad',
                 'id': '8bc8562f52',
                 'EMAIL': base.inputFormData.email,
                 'group[13605]': base.inputFormData.city,
                 'OTHERCITY': base.inputFormData.otherCity,
                 'subscribe': 'Subscribe',
                 'group[13601][1]': 1
             };

             if (base.inputFormData.city != base.otherCityValue) {
                 postFormData.OTHERCITY = base.inputFormData.otherCity;
             }
             base.traSubs();

             if (base.options.memberhipTrack) {
                 base.tracMeberships();
             }

             $.ajax({
                 method: 'GET',
                 url: 'https://brunchwork.us11.list-manage.com/subscribe/post-json',
                 data: postFormData,
                 cache: false,
                 dataType: "jsonp",
                 // processData:false,
                 // dataType: 'json', 
                 encode: true,
                 jsonp: "c", // trigger MailChimp to return a JSONP response
                 contentType: "application/json; charset=utf-8",
                 success: function(data) {
                     console.log('data', data);
                     base.setLoading(false);
                     if (data.result == 'error') {
                         base.responseBlockSuccess('');
                         jQuery('#' + base.uniqueIdentifier + 'responseBlock').removeClass('hide-error');
                         jQuery('#' + base.uniqueIdentifier + 'otherCityFromGroup').css('display', 'none');
                         base.responseBlockError(data.msg);
                     } else {
                         base.responseBlockError('');
                         jQuery('#' + base.uniqueIdentifier + 'responseBlock').removeClass('hide-error');
                         jQuery('#' + base.uniqueIdentifier + 'otherCityFromGroup').css('display', 'none');
                         base.responseBlockSuccess(data.msg);
                         window.open('https://brunchwork.brandfuss.com/referral-promotion-new/?email=' + base.inputFormData.email + '&city=' + base.inputFormData.citytext + '&other_city=' + base.inputFormData.otherCity, '_blank');
                     }
                 },
                 error: function(error) {
                     base.setLoading(false);
                     base.responseBlockSuccess('');
                     base.responseBlockError('Unable to subscribe please try again later ');
                     console.log('error', error);
                 }
             });


         };

         // events liteners added for the susbscribe on change event 
         base.subscribeClickHandler = function(subscrieClickEvent) {
             base.logMessage('subscribe click event clicked');

             if (base.getLoading()) {
                 base.logMessage('subscribtion is already in proccess ');
                 return;
             }

             base.setLoading(true);
             base.getFormData();
             if (base.validateSubscriberForm()) {
                 base.logMessage('Form is Valid ');
                 base.callMailchimpAPI();
             } else {
                 base.logMessage('Form is Invalid ');
                 base.setLoading(false);
             }

         };

         base.cityChangeHandler = function(cityChangeEvent) {
             base.logMessage('City Changed Event');
             if (base.bwformEle.$citySelect.val() != base.otherCityValue) {
                 base.bwformEle.$otherCityFromGroup.hide();
             } else {
                 base.bwformEle.$otherCityFromGroup.show();
             }
         };

         base.resetSubscribeForm = function() {
             base.bwformEle.$otherCityFromGroup.hide();
             base.setEmailError('');
             base.setCityError('');
             base.setOtherCityError('');
             base.responseBlockError('');
             base.responseBlockSuccess('');
             // base.bwformEle.$emailInput.val('raj@tempinbox.com');
             // base.bwformEle.$citySelect.val('4');
         };

         base.linkEvents = function() {
             base.logMessage('linking even listeners');
             base.bwformEle.$susbscribeEmailForm = $('#' + base.bwformEle.susbscribeEmailForm);
             base.bwformEle.$emailFromGroup = $('#' + base.bwformEle.emailFromGroup);
             base.bwformEle.$emailInput = $('#' + base.bwformEle.emailInput);
             base.bwformEle.$emailHelpBlock = $('#' + base.bwformEle.emailHelpBlock);
             base.bwformEle.$cityFromGroup = $('#' + base.bwformEle.cityFromGroup);
             base.bwformEle.$citySelect = $('#' + base.bwformEle.citySelect);
             base.bwformEle.$cityHelpBlock = $('#' + base.bwformEle.cityHelpBlock);
             base.bwformEle.$otherCityFromGroup = $('#' + base.bwformEle.otherCityFromGroup);
             base.bwformEle.$otherCityInput = $('#' + base.bwformEle.otherCityInput);
             base.bwformEle.$otherCityHelpBlock = $('#' + base.bwformEle.otherCityHelpBlock);
             base.bwformEle.$susbscribeButton = $('#' + base.bwformEle.susbscribeButton);
             base.bwformEle.$responseBlock = $('#' + base.bwformEle.responseBlock);
             base.bwformEle.$responseBlockFromGroup = $('#' + base.bwformEle.responseBlockFromGroup);

             base.bwformEle.$susbscribeButton.on('click', base.subscribeClickHandler);
             base.bwformEle.$citySelect.on('change', base.cityChangeHandler);

         };


         base.getOutHtmlWithUniqueIdentifier = function(uniqueIdentifier) {

             base.bwformEle = {};

             // add jquery elements to the modal form 
             base.bwformEle.susbscribeEmailForm = uniqueIdentifier + 'susbscribeEmailForm';
             base.bwformEle.emailFromGroup = uniqueIdentifier + 'emailFromGroup';
             base.bwformEle.emailInput = uniqueIdentifier + 'emailInput';
             base.bwformEle.emailHelpBlock = uniqueIdentifier + 'emailHelpBlock';
             base.bwformEle.cityFromGroup = uniqueIdentifier + 'cityFromGroup';
             base.bwformEle.citySelect = uniqueIdentifier + 'citySelect';
             base.bwformEle.cityHelpBlock = uniqueIdentifier + 'cityHelpBlock';
             base.bwformEle.otherCityFromGroup = uniqueIdentifier + 'otherCityFromGroup';
             base.bwformEle.otherCityInput = uniqueIdentifier + 'otherCityInput';
             base.bwformEle.otherCityHelpBlock = uniqueIdentifier + 'otherCityHelpBlock';
             base.bwformEle.susbscribeButton = uniqueIdentifier + 'susbscribeButton';
             base.bwformEle.responseBlock = uniqueIdentifier + 'responseBlock';
             base.bwformEle.responseBlockFromGroup = uniqueIdentifier + 'responseBlockFromGroup';

             return '<div id="' + base.bwformEle.susbscribeEmailForm + '" action="#" class="wpcf7-form signupFrom clearfix mailchimp-ext-0.4.45">' +
                 '    <div class="wpcf7-form-control-wrap city" id="' + base.bwformEle.cityFromGroup + '">' +
                 '        <select name="group[13605]" id="' + base.bwformEle.citySelect + '" class="form-control">' +
                 '            <option value="">CITY</option>' +
                 '            <option value="4">NYC</option>' +
                 '            <option value="8">SF</option>' +
                 '            <option value="256">LA</option>' +
                 '            <option value="16">BOS</option>' +
                 '            <option value="1024">CHI</option>' +
                 '            <option value="64">DC</option>' +
                 '            <option value="512">AUS</option>' +
                 '            <option value="262144">MIAMI</option>' +
                 '            <option value="128">Other</option>' +
                 '        </select>' +
                 '        <span id="' + base.bwformEle.cityHelpBlock + '" class="hide-error help-block"> </span>' +
                 '    </div>' +

                 '    <div class="wpcf7-form-control-wrap your-email" id="' + base.bwformEle.emailFromGroup + '">' +
                 '        <input placeholder="ENTER YOUR EMAIL" type="email" class="form-control" id="' + base.bwformEle.emailInput + '" name="EMAIL">' +
                 '        <span id="' + base.bwformEle.emailHelpBlock + '" class="hide-error help-block"> </span>' +
                 '    </div>' +
                 '        <button id="' + base.bwformEle.susbscribeButton + '" class="wpcf7-form-control wpcf7-submit"> ' + base.options.subscribetext + ' </button>' +
                 '    <div class="form-group" id="' + base.bwformEle.responseBlockFromGroup + '">' +
                 '        <span id="' + base.bwformEle.responseBlock + '" class="hide-error help-block"> </span>' +

                 '    </div><div style="clear:both"></div>' +
                 '   <div class="form-group" style="margin:auto;text-align: center;" id="' + base.bwformEle.otherCityFromGroup + '">' +
                 '        <input placeholder="OTHER CITY" type="text" class="other_bottom" id="' + base.bwformEle.otherCityInput + '" name="OTHERCITY">' +
                 '        <span id="' + base.bwformEle.otherCityHelpBlock + '" class=" hide-error help-block"> </span>' +
                 '    </div>' +
                 '</div>';
         };
         // Sample Function, Uncomment to use
         base.logMessage = function(logMessage, logValue) {
             logMessage = base.pluginName + ': ' + logMessage;
             if (typeof logValue === typeof undefined) {
                 console.log(logMessage);
             } else {
                 console.log(logMessage, logValue);
             }

         };
         // Run initializer
         base.init();
     };

     $.bw.mailChimpSubscribeModal.defaultOptions = {
         subscribetext: " Subscribe",
         memberhipTrack: false
     };

     $.fn.bw_mailChimpSubscribeModal = function(uniqueIdentifier, options) {
         return this.each(function() {
             (new $.bw.mailChimpSubscribeModal(this,
                 uniqueIdentifier, options));
         });
     };

 })(jQuery);